mindtext 中文文档
=====================
`mindtext <https://gitee.com/mindspore/mindtext/>`_ 是一款轻量级的自然语言处理（NLP）工具包。你既可以用它来快速地完成一个NLP任务，
也可以用它在研究中快速构建更复杂的模型。

用户手册
----------------

.. toctree::
   :maxdepth: 2

    安装指南 </user/installation>
    快速入门 </user/quickstart>
    详细教程 </user/tutorials>

API 文档
-------------

除了用户手册之外，你还可以通过查阅 API 文档来找到你所需要的工具。

.. toctree::
   :titlesonly:
   :maxdepth: 4
   
   mindtextAPI </apis/api>


