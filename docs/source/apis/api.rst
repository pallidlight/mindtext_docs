﻿========================
mindtext API
========================

这里是更详细的使用教程，可以选择你只想了解其中的一部分进行选读。

子模块
------

.. toctree::
   :maxdepth: 1

   mindtext.common </apis/mindtext.common/index>
   mindtext.embeddings </apis/mindtext.embeddings/index>
   mindtext.dataset </apis/mindtext.dataset/index>
   mindtext.modules </apis/mindtext.modules/index>
