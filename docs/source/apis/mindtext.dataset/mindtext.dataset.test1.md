# mindtext.dataset.test1  

Dataset的构建  
=

Example SST-2数据集Dataset构建

    from mindtext.dataset.classification import SST2Dataset

    dataset = SST2Dataset(paths='./mindtext/dataset/SST-2',
                          tokenizer="./mindtext/pretrain/roberta-base",
                          max_length=128,
                          truncation_strategy=True,
                          batch_size=32)
    
    ds = dataset() 
    ds = dataset.from_cache( columns_list=['input_ids', 'attention_mask','label'],
                          test_columns_list=['input_ids', 'attention_mask'],
                          batch_size=32
                          )
    
    train_dataset = ds['train']
mindtext.dataset.base_dataset.Dataset
--  
>  class mindtext.dataset.base_dataset.Dataset( vocab (Vocabulary, Optional): Convert tokens to index,default None.
>  name (str, Optional): Dataset name,default None.
>  label_map (Dict[str, int], Optional): Dataset label map,default None.)  

通过base_dataset中基类Dataset来构建文本分类、文本匹配和生成任务对应的数据集  

>init(self, vocab: Vocabulary = None, name: str = None,
                 label_map: Dict[str, int] = None)

参数
> + vocab(Vocabulary):  词表，默认为None  
> + name(str):  下游任务数据集Dataset名称，默认为None  
> + label_map(Dict[str, int], Optional):Dataset标签映射
> 

mindtext.dataset.base_dataset.CLSBaseDataset
--

文本分类Dataset的基类

Example
>class SST2Dataset(CLSBaseDataset):

mindtext.dataset.base_dataset.PairCLSBaseDataset
-

文本匹配Dataset基类  

Example  

>class LCQMCDataset(PairCLSBaseDataset): 