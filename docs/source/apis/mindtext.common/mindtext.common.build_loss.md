# mindtext.common.build_loss

> mindtext.common.loss.builder.py

使用builder.py文件配置config文件里设定使用的loss函数，这里使用的是注册调用机制。

Example

```python
from mindtext.common.loss.builder import build_loss

network_loss = build_loss(config.loss)
```

* config.loss是在config参数文件中设置的loss函数
