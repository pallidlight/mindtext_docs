# mindtext.common.build_optimizer

> mindtext.common.optimizer.builder.py

使用builder.py文件配置config文件里设定使用的优化器，这里使用的是注册调用机制。

Example

```python
from mindvision.common.optimizer.builder import build_optimizer

network_opt = build_optimizer(config.optimizer)
```

* config.optimizer是在config参数文件中设置的优化器
