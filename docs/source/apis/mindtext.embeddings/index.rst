mindtext.embeddings
============

.. automodule:: mindtext.embeddings

子模块
------

.. toctree::
   :maxdepth: 1

   mindtext.embeddings.char_embedding
   mindtext.embeddings.embedding
   mindtext.embeddings.static_embedding

