# mindtext.embeddings.embedding

该模块中的`Embedding`是一种随机初始化的`embedding`，我们更推荐您使用`mindtext.embeddings.static_embedding`，
或基于预训练权重初始化的`Embedding`。

> _class_ __mindtext.embeddings.embedding.Embedding__ (_init_embed: Union[Tuple[int, int], np.ndarray, mindspore.Tensor, nn.Cell], dropout: float = 0.1_)

词嵌入，支持多种方式的输入初始化。您可以使用`self.num_embeddings`获取词表大小，使用`self.embedding_dim`获取嵌入的维度

Example

```shell
>>> import numpy as np
>>> init_embed = (200, 100)
>>> embedding = Embedding(init_embed, 0.1)
>>> init_embed = np.random.randn(4, 3)
>>> embedding = Embedding(init_embed, 0.1)
```

> **init** (_init_embed: Union[Tuple[int, int], np.ndarray, mindspore.Tensor, nn.Cell], dropout: float = 0.1_)

参数

* __init_embed__ (_Union[Tuple[int, int], np.ndarray, mindspore.Tensor, nn.Cell]_): 词嵌入的大小，传入tuple(int, int), 第一
个int为vocab_zie（词典大小），第二个int为embed_dim（嵌入维度大小）。若传入np.ndarray, mindspore.Tensor, nn.Cell，则直接使用该值初
始化词嵌入。
* __dropout__ (_float_): 对词嵌入的输出进行dropout。

> **construct** (_words: mindspore.Tensor_)

参数

* __words__ (_mindspore.Tensor_): 单词的tokens，shape为(batch_size, seq_len)。

返回

* __mindspore.Tensor__: 词嵌入的输出，shape为(batch_size, seq_len, embed_dim)。

> _class_ __mindtext.embeddings.embedding.TokenEmbedding__ (_vocab: Vocabulary, dropout: float = 0.1_)

mindtext中各种Embedding的基类。

> __init__ (_vocab: Vocabulary, dropout: float = 0.1_)

参数

* __vocab__ (_Vocabulary_): 词表。
* __dropout__ (_float_): 对Embedding后的输出进行dropout，默认值为0.1。

> __get_word_vocab__ ()

返回embedding的词典。

返回： Vocabulary

> __dropout__ (_words: mindspore.Tensor_)

参数

* __words__ (_mindspore.Tensor_): 对embedding后的词向量表示进行dropout。

返回

* __mindspore.Tensor__: dropout后的词向量表示。

> num_embeddings ()

返回embedding的单词数量（即词典大小），这个值可能会大于实际的embedding矩阵的大小。
