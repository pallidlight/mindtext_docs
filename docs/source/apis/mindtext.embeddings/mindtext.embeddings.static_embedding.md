# mindtext.embeddings.static_embedding

> class __mindtext.embeddings.static_embedding.StaticEmbedding__(_vocab: Vocabulary, model_dir_or_name: Union[str, None] = None, embedding_dim=-1,requires_grad: bool = True, dropout=0.1_)

给定预训练embedding的名称或路径，根据vocab从embedding中抽取相应的数据。如果没有找到相应的数据，则会随机初始化embedding。

Example

```shell
>>> vocab = Vocabulary()
>>> vocab.update(["i", "am", "fine"])
>>> embed = StaticEmbedding(vocab, model_dir_or_name=None, embedding_dim=5)
>>> words = mindspore.Tensor([[vocab[word] for word in ["i", "am", "fine"]]])
>>> words_emb = embed(words)
>>> Tensor(shape=[1, 3, 5], dtype=Float32, value=
            [[[7.56267071e-001, -3.02625038e-002, 6.10783875e-001, 4.03315663e-001, -6.82987273e-001],
            [3.35875869e-001, 2.93195043e-002, 2.17977986e-001, 9.68403295e-002, -4.01605248e-001],
            [-2.35586300e-001, 4.89649743e-001, -2.10691467e-001, -1.81295246e-001, -6.90823942e-002]]]).
```

> __init__ (_vocab: Vocabulary, model_dir_or_name: Union[str, None] = None, embedding_dim=-1,requires_grad: bool = True, dropout=0.1_)

参数

* __vocab__ (_Vocabulary_): Vocabulary对象。 StaticEmbedding只会加载单词列表中包含的单词的词向量，如果在预训练的Embedding中没有找到，则使用随机初始化的Embedding。

* __model_dir_or_name__ (_Union[str, None]_): 预训练的StaticEmbedding有两种调用方式：1）是传入本地文件夹（一个后缀为`.txt`的文件）或文件路径。 2）是传入embedding的名称。 在第2种情况下，StaticEmbedding会自动检查模型是否存在于缓存中。 如果没有，StaticEmbedding会自动下载（华为云实现后）。如果输入为`None`，则使用维度为`embedding_dim`的随机初始化embedding。默认值为`None`。

* __embedding_dim__ (_int_): 随机初始化embedding的维度。如果值大于0，`model_dir_or_name`将被忽略。

* __requires_grad__ (_bool_): 是否允许优化Embedding的参数。若为`True`，则优化所有参数；若为`False`，所有参数不允许优化；若为`None`，则部分参数允许优化、部分不允许。默认值为`True`。

* __dropout__ (_float_): 对embedding后的输出表示进行dropout。默认值为`0.1`。

> __construct__ (_words_)

传入单词的tokens，返回对应的embedding表示

参数

* __words__ (_mindspore.Tensor_): 单词的tokens，shape为(batch_size, seq_len)。

返回

* __mindspore.Tensor__：单词的embedding表示，shape为(batch_size, seq_len)。
