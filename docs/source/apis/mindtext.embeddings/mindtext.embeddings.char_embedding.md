# mindtext.embeddings.char_embedding

该文件包含的是针对character的Embedding，是基于CNN的character embedding。与其它Embedding一样，这里的输入也是单词的tokens而不需要使用单词中字符的tokens来获取对应的字符嵌入表示。

> class __mindtext.embeddings.char_embedding.CNNCharEmbedding__ (_vocab: Vocabulary, embed_size: int = 50, char_emb_size: int = 50,
            dropout: float = 0.1, filter_nums: List[int] = (30,), kernel_sizes: List[int] = (3,),
            pool_method: str = 'max', activation: str = 'relu', min_char_freq: int = 1,
            pre_train_char_embed: Optional[str] = None, requires_grad: bool = True,
            include_word_start_end: bool = True_)

使用卷积神经网络（CNN）生成字符嵌入（char embedding）。其结构为：embedding(x) -> dropout(x) -> CNN(x) -> activation(x) -> pool(x) -> fc(x) -> Dropout(x)。对于不同的kernel大小的fitler，其结果是拼接起来，然后通过一层全连接层，最后输出字符嵌入表示。

Example

```shell
>>> import mindspore
>>> from mindtext.common.data.vocabulary import Vocabulary
>>> from mindtext.embeddings.char_embedding import CNNCharEmbedding
>>> vocab = Vocabulary()
>>> vocab.update("The whether is good .".split())
>>> vocab.build_vocab()
>>> embed = CNNCharEmbedding(vocab, embed_size=50)
>>> words = mindspore.Tensor([[vocab.to_index(word) for word in "The whether is good .".split()]])
>>> outputs = embed(words)
>>> outputs.shape
>>> # (1, 5, 50)
```

> __init__ (_vocab: Vocabulary, embed_size: int = 50, char_emb_size: int = 50,
            dropout: float = 0.1, filter_nums: List[int] = (30,), kernel_sizes: List[int] = (3,),
            pool_method: str = 'max', activation: str = 'relu', min_char_freq: int = 1,
            pre_train_char_embed: Optional[str] = None, requires_grad: bool = True,
            include_word_start_end: bool = True_)

参数

* __vocab__ (_Vocabulary_): 词表。
* __embed_size__ (_int_): character embedding的输出维度大小，默认值为`50`。
* __char_emb_size__ (_int_): 字符嵌入的维度大小。字符是从vocab中生成的。默认值为`50`。
* __dropout__ (_float_): 对CNNCharEmbedding后的输出进行dropout。默认值为`0.1`。
* __filter_nums__ (_List[int]_): filter的数量。长度需要和kernels一致。默认值为`(30,)`。
* __kernel_sizes__ (_List[int]_): 卷积核的大小。默认值为`(3,)`。
* __pool_method__ (_str_): 把字符表示合成为一个表示时所用的池化方法。支持最大池化`max`和平均池化`avg`。默认值为`max`。
* __activation__ (_str_): 激活函数，支持`relu`，`sigmoid`，`tanh`或者自定义函数。默认值`relu`。
* __min_char_freq__ (_int_)：字符出现的最少次数。默认值为`1`。
* __pre_train_char_embed__ (_str, optional_): 有两种方式调用预训练好的character embedding：1）是传入embedding文件夹 (一个以`.txt`作为后缀的文件)或文件路径；2）是传入embedding的名称，第2种情况将自动查看缓存中是否存在该模型，没有的话将自动下载（华为云实现后）。如果输入为`None`则使用`embedding_dim`的维度随机初始化一个embedding。
* __requires_grad__  (_bool_): 是否更新权重。默认值为`True`。
* __include_word_start_end__ (_bool_): 是否在每个单词开始的字符前和结束的字符前增加特殊符合。

> __construct__ (_words: mindspore.Tensor_)：

输入words的tokens后，生成对应的字符嵌入表示。

参数：

* __words__ (_mindspore.Tensor_): 单词的tokens，shape为(batch_size, seq_len)。

返回：

* __mindspore.Tensor__: 字符嵌入表示，shape为(batch_size, seq_len, embed_size)。
