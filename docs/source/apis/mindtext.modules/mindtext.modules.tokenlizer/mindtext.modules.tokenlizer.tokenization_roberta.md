# mindtext.modules.tokenlizer.tokenization_roberta

> __bytes_to_unicode__ ()

返回
* __dict__(_zip_)：返回utf-8的列表和一个到Unicode的映射。

> __get_pairs__ (_word: tuple_)

返回word中的字符对集

参数 
* __word__ (_tuple_): 字符的一个输入元组。

返回
* __pairs__()：可变长度里的字符。

> __roberta_tokenize__ (_text: str, add_prefix_space=True_)

base robert的tokenize

参数 
* __text__ (_str_): 输入文本。
* __add_prefix_space__ (_bool_): 当True的时候确保和Roberta在训练时一样。

返回
* __tokens__ (_list_)：token的列表。

> _class_ __mindtext.modules.tokenlizer.tokenization_roberta.RobertaTokenizer__ _(vocab_file, merges_file, errors="replace", 
> bos_token="s>", eos_token="/s>", sep_token="/s>", cls_token="s>", unk_token="<unk>", pad_token="<pad>", mask_token="<mask>",
> **kwargs)_

> __init__ (_vocab_file, merges_file, errors="replace", bos_token="s>", eos_token="/s>", sep_token="/s>", cls_token="s>",
> unk_token="<unk>", pad_token="<pad>", mask_token="<mask>",**kwargs_)

参数
* __vocab_file__ (_int_): vacab文件的路径。
* __merges_file__ (_float_): merges文件的路径，merge文件包含所欲的token。
* ___token__ (): 特殊字符。


> __bpe__ (_token: list_)

字节层面的字节对编码

Example
```
>>> the spaces at the beginning of a string:
>>>tokenizer.decode(tokenizer.encode(" Hello")) = "Hello"
```

参数 
* __token__ (_list_): 输入的token。

返回
* __word__(_tuple_)：token的元组。

> ___tokenize__ (_text: str, add_prefix_space=False_)

对一个字符串进行tokenize

参数 
* __text__ (_str_): 输入的文本。
* __add_prefix_space__ (_bool_): 至少以一个空格开始一个句子以保证在RoBERTa tokenizers的顺序不变，默认为False


返回
* __bpe_tokens__(_tuple_)：BPE的tokens。

> __convert_tokens_to_string__ (_token: list_)

将一个tokens的序列转化为一个单独的字符串

参数 
* __token__ (_list_): 输入的token。

返回
* __text__(_list_)：输出的字符串。

> __tokenize__ (_text: str, add_prefix_space=True_)

将一个字符串转换成一序列的tokens

参数 
* __text__ (_str_): 要被编码的序列。
* __add_prefix_space__ (_bool_): 至少以一个空格开始一个句子以保证在RoBERTa tokenizers的顺序不变，默认为True。

返回
* __tokenized_text__(_list_)：被tokenized的文本。

> __convert_tokens_to_ids__ (_token: list_)

将一个token使用字典转化为一个单独的整数

参数 
* __token__ (_list_): 输入的token。

返回
* __id__(_list_)：转换完的id。

> __convert_ids_to_tokens__ (_ids: list, skip_special_tokens=False_)

将一个单独的索引或者是一序列的索引转化成token，使用字典和添加的tokens

参数 
* __skip_special_tokens__ (_bool_): 不要解码特殊字符。

返回
* __token__(_list_)：转换完的token。

> __convert_id_to_tokens__ (_token_ids, skip_special_tokens=False, clean_up_tokenization_spaces=True_)

使用字典将一序列的ids转化成一个字符串。

返回
* __token__(_list_)：转换完的token。

> __encode__ (_text: str, add_special_tokens=False, add_prefix_space=True_)

给定一个文本输入，编码成索引格式的数据

Example
```
>>> from mindtext.modules.tokenlizer import tokenization_roberta
>>>roberta_tokenizer = tokenization_roberta.from_pretrained('en')
>>>print(tokenization_roberta.encode('from'))
>>>print(tokenization_roberta.encode("This is a demo sentence"))
>>>print(tokenization_roberta.encode(["This", "is", 'a']))
```

参数 
* __text__ (_str_): 输入的text。
* __add_special_tokens__ (_bool_): 是否确保句子的开始或者结束token是CLS和SEP。
* __add_prefix_space__ (_bool_): 至少以一个空格开始一个句子以保证在RoBERTa tokenizers的顺序不变，默认为True。

返回
* __word_pieces__(_tuple_)：索引格式的数据。

> __get_used_merge_pair_vocab__ (_token: list_)

如果token没有被找到，他会被分成字母返回

Example
```
>>> If the word is "abcd"，it will  split into((a,b), (b,c), (c, d), (e,f))
```

参数 
* __token__ (_lsit_): 输入的token。

返回
* __word__(_tuple_)：分隔好的word。
* __used_pairs__(_dict_)：最常见的对。