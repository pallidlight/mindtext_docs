mindtext.embeddings
============

.. automodule:: mindtext.modules.tokenlizer

子模块
------

.. toctree::
   :maxdepth: 1

   mindtext.modules.tokenlizer.tokenization_roberta
   mindtext.modules.tokenlizer.tokenization_transformer

