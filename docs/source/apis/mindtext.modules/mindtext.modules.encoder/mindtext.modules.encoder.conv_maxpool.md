# mindtext.modules.encoder.conv_maxpool

> _class_ __mindtext.modules.encoder.conv_maxpool.ConvMaxpool__ _(in_channels, out_channels, kernel_size, stride=1, padding=0, 
> has_bias=False, activation="relu")_

一个包含的那个卷积和最大池化的编码层

Example
```
>>> conv_maxpool = ConvMaxpool(in_channels=128, out_channels=3, kernel_sizes=3)
>>> example = Tensor(np.random.rand(2, 128, 128), mindspore.float32)
>>> output = conv_maxpool(example)
```

> __init__ (_in_channels, out_channels, kernel_size, stride=1, padding=0, has_bias=False, activation="relu"_)

参数
* __in_channels__ (_int_): 输入的大小，通常为编码大小。
* __out_channels__ (_int_): 输出的大小。
* __kernel_size__ (_int_): 卷积核的大小。
* __stride__ (_int_): 卷积步长。
* __padding__ (_int_): 填充的值。
* __has_bias__ (_bool_): 是否使用偏置。
* __activation__ (_str_): 激活函数，默认为relu。

> __construct__ (_x_)
 
参数
* __x__ (_Tensor_): batch_size x max_len x input_size。

返回
* __x__ (_Tensor_): 卷积最大池化编码层返回的向量。