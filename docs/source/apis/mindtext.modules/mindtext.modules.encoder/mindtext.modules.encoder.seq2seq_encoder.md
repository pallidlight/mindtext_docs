# mindtext.modules.encoder.seq2seq

> _class_ __mindtext.modules.encoder.seq2seq.FeedForward__ _(in_channels: int,
                 hidden_size: int,
                 out_channels: int,
                 hidden_act: str = "relu",
                 hidden_dropout_prob: float = 0.1,
                 compute_type: mindspore.dtype = mstype.float32)_

> __init__ (_in_channels: int,
                 hidden_size: int,
                 out_channels: int,
                 hidden_act: str = "relu",
                 hidden_dropout_prob: float = 0.1,
                 compute_type: mindspore.dtype = mstype.float32_) 

参数
* __hidden_size__ (_int_): 隐藏层大小。
* __in_channels__ (_int_): 输入层的大小。
* __out_channels__ (_int_): 输出层的大小。
* __hidden_act__ (_str_): bert的激活函数，默认为“relu”。
* __hidden_dropout_prob__ (_float): dropout的大小，默认为0.1。
* __compute_type__ (_mindspore.dtype_): Bert模型的计算数据类型，默认为mstype.float32。

> __construct__ (_input_tensor: mindspore.Tensor_)

参数
* __input_tensor__ (_mindspore.Tensor_): 前馈神经网络的输入。


返回
* __output__ (_mindspore.Tensor_): 前馈神经网络的输出。

> _class_ __mindtext.modules.encoder.seq2seq.EncoderCell__ _(batch_size: int,
                 hidden_size: int = 1024,
                 num_attention_heads: int = 16,
                 intermediate_size: int = 4096,
                 attention_probs_dropout_prob: float = 0.1,
                 use_one_hot_embeddings: bool = False,
                 initializer_range: float = 0.02,
                 hidden_dropout_prob: float = 0.1,
                 hidden_act: str = "relu",
                 compute_type: mindspore.dtype = mstype.float32)_


> __init__ (_batch_size: int,
                 hidden_size: int = 1024,
                 num_attention_heads: int = 16,
                 intermediate_size: int = 4096,
                 attention_probs_dropout_prob: float = 0.1,
                 use_one_hot_embeddings: bool = False,
                 initializer_range: float = 0.02,
                 hidden_dropout_prob: float = 0.1,
                 hidden_act: str = "relu",
                 compute_type: mindspore.dtype = mstype.float32_)

参数
* __batch_size__ (_int_): 输入数据集的batch size。
* __hidden_size__ (_int_): 编码层的大小，默认为1024。
* __num_attention_heads__ (_int_): 注意力头数量，默认为16。
* __intermediate_size__ (_int_): 中间层的隐藏维度大小，默认为4096。
* __attention_probs_dropout_prob__ (_float_): BertAttention的dropout概率，默认为0.02。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码格式，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __hidden_dropout_prob__ (_float): dropout的大小，默认为0.1。
* __hidden_act__ (_str_): bert的激活函数，默认为“relu”。
* __compute_type__ (_mindspore.dtype_): Bert模型的计算数据类型，默认为mstype.float32。

> __construct__ (_hidden_states: mindspore.Tensor, attention_mask: mindspore.Tensor, seq_length: int_)

参数
* __hidden_states__ (_mindspore.Tensor_): 编码层的隐藏状态，shape为(batch_size, seq_len, hidden_size)。
* __attention_mask__ (_mindspore.Tensor_): attention_mask。
* __seq_length__ (_int_): 输入序列的长度。

返回
* __output__ (_mindspore.Tensor_): encoder的输出。

> _class_ __mindtext.modules.encoder.seq2seq.TransformerEncoder__ _(batch_size: int,
                 hidden_size: int,
                 num_hidden_layers: int,
                 num_attention_heads: int = 16,
                 intermediate_size: int = 4096,
                 attention_probs_dropout_prob: float = 0.1,
                 initializer_range: float = 0.02,
                 hidden_dropout_prob: float = 0.1,
                 hidden_act: str = "relu",
                 compute_type: mindspore.dtype = mstype.float32)_


> __init__ (_batch_size: int,
                 hidden_size: int,
                 num_hidden_layers: int,
                 num_attention_heads: int = 16,
                 intermediate_size: int = 4096,
                 attention_probs_dropout_prob: float = 0.1,
                 initializer_range: float = 0.02,
                 hidden_dropout_prob: float = 0.1,
                 hidden_act: str = "relu",
                 compute_type: mindspore.dtype = mstype.float32_)

参数
* __batch_size__ (_int_): 输入数据集的batch size。
* __hidden_size__ (_int_): 编码层的大小。
* __num_hidden_layer__ (_int_): 编码层的隐藏层数量。
* __num_attention_heads__ (_int_): 注意力头数量，默认为16。
* __intermediate_size__ (_int_): 中间层的隐藏维度大小，默认为4096。
* __attention_probs_dropout_prob__ (_float_): BertAttention的dropout概率，默认为0.1。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __hidden_dropout_prob__ (_float): dropout的大小，默认为0.1。
* __hidden_act__ (_str_): bert的激活函数，默认为“gelu”。
* __compute_type__ (_mindspore.dtype_): Bert模型的计算数据类型，默认为mstype.float32。

> __construct__ (_hidden_states: mindspore.Tensor, attention_mask: mindspore.Tensor, seq_length: int_)

参数
* __hidden_states__ (_mindspore.Tensor_): 编码层的隐藏状态，shape为(batch_size, seq_len, hidden_size)。
* __attention_mask__ (_mindspore.Tensor_)：自注意力隐藏状态的mask矩阵（2D或者3D），值是[0/1]或者[True/False]，shape为(seq_len, seq_len)或者(batch_size, seq_len, seq_len)。
* __seq_length__ (_int_)：输入的长度。

返回
* __output__ (_mindspore.Tensor_): encoder的输出。
