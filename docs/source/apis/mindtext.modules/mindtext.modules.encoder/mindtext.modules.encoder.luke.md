# mindtext.modules.encoder.luke

> _class_ __mindtext.modules.encoder.luke.LukeConfig__ _(seq_length=256,
                 vocab_size=50265,
                 hidden_size=1024,
                 num_hidden_layers=24,
                 num_attention_heads=16,
                 intermediate_size=4096,
                 hidden_act="gelu",
                 hidden_dropout_prob=0.1,
                 attention_probs_dropout_prob=0.1,
                 max_position_embeddings=514,
                 type_vocab_size=1,
                 initializer_range=0.02,
                 batch_size=2,
                 use_relative_positions=False,
                 dtype=mstype.float32,
                 compute_type=mstype.float32,
                 bert_model_name="roberta-large",
                 bos_token_id=0,
                 entity_emb_size=256,
                 entity_vocab_size=500000,
                 eos_token_id=2,
                 gradient_checkpointing=False,
                 layer_norm_eps=1e-05,
                 model_type="luke",
                 output_past=True,
                 pad_token_id=1,
                 position_embedding_type="absolute")_

luke模型的配置信息

> __init__ (_seq_length=256,
                 vocab_size=50265,
                 hidden_size=1024,
                 num_hidden_layers=24,
                 num_attention_heads=16,
                 intermediate_size=4096,
                 hidden_act="gelu",
                 hidden_dropout_prob=0.1,
                 attention_probs_dropout_prob=0.1,
                 max_position_embeddings=514,
                 type_vocab_size=1,
                 initializer_range=0.02,
                 batch_size=2,
                 use_relative_positions=False,
                 dtype=mstype.float32,
                 compute_type=mstype.float32,
                 bert_model_name="roberta-large",
                 bos_token_id=0,
                 entity_emb_size=256,
                 entity_vocab_size=500000,
                 eos_token_id=2,
                 gradient_checkpointing=False,
                 layer_norm_eps=1e-05,
                 model_type="luke",
                 output_past=True,
                 pad_token_id=1,
                 position_embedding_type="absolute"_)

参数
* __seq_length__ (_int_): 输入序列的的长度，默认为256。
* __vocab_size_ (_int_): 每个embedding向量的shape，默认为50265。
* __hidden_size__ (_int_): bert编码器的大小，默认为1024。
* __num_hidden_layer__ (_int_): 隐藏层的数量，默认为24。
* __num_attention_heads__ (_int_): 在BertTransformer编码器里面每一个attention层的注意力头数量，默认为16。
* __intermediate_size__ (_int_): 中间层的隐藏维度大小，默认为4096。
* __hidden_act__ (_str_): bert的激活函数，默认为“gelu”。
* __hidden_dropout_prob__ (_float): dropout的大小，默认为0.1。
* __attention_probs_dropout_prob__ (_float_): BertAttention的dropout概率，默认为0.1。
* __max_position_embeddings__ (_int_): 用于模型的最大序列长度，默认为514。
* __type_vocab_size__ (_int_): 典型字典的大小，默认为1。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __batch_size__ (_int_): 数据集的batch size，默认为2。
* __use_relative_positions__ (_bool_): 是否使用关系位置，默认为False。
* __dtype__ (_mindspore.dtype_): 数据类型，默认为mstype.float32。
* __compute_type__ (_mindspore.dtype_): Bert模型的计算数据类型，默认为mstype.float32。

> _class_ __mindtext.modules.encoder.luke.LukeConfig__ _(config)_

luke模型

> __init__ (_config_)

参数 
* __config__ (_LukeConfig_): LukeConfig。

> _class_ __mindtext.modules.encoder.luke.LukeEntityAwareAttentionModel__ _(config)_


> __init__ (_config)_)

参数 
* __config__ (_LukeConfig_): LukeConfig。

> __construct__ (_word_ids, word_segment_ids, word_attention_mask, entity_ids, entity_position_ids, entity_segment_ids, entity_attention_mask_)

参数
* __word_ids__ (_mindspore.Tensor_): word对应的字典序号。
* __word_segment_ids__ (_mindspore.Tensor_)：word_segment_ids。
* __word_attention_mask__ (_mindspore.Tensor_)：word_attention_mask。
* __entity_ids__ (_mindspore.Tensor_)：entity_ids。
* __entity_position_ids__ (_mindspore.Tensor_)：entity_position_ids。
* __entity_segment_ids__ (_mindspore.Tensor_)：entity_segment_ids。
* __entity_attention_mask__ (_mindspore.Tensor_)：entity_attention_mask。

返回
* __output__ (_mindspore.Tensor_): luke的输出。

> _class_ __mindtext.modules.encoder.luke.EntityAwareSelfAttention__ _(config)_


> __init__ (_config_)

参数 
* __config__ (_LukeConfig_): LukeConfig。

> __construct__ (_word_hidden_states, entity_hidden_states, attention_mask_)

参数
* __word_hidden_states__ (_mindspore.Tensor_): word的隐藏状态。
* __entity_hidden_states__ (_mindspore.Tensor_)：实体的隐藏状态。
* __attention_mask__ (_mindspore.Tensor_)：attention_mask。

返回
* __context_layer__ (_mindspore.Tensor_): 输出。

> _class_ __mindtext.modules.encoder.luke.EntityAwareAttention__ _(config)_


> __init__ (_config_)

参数 
* __config__ (_LukeConfig_): LukeConfig。

> __construct__ (_word_hidden_states, entity_hidden_states, attention_mask_)

参数
* __word_hidden_states__ (_mindspore.Tensor_): word的隐藏状态。
* __entity_hidden_states__ (_mindspore.Tensor_)：实体的隐藏状态。
* __attention_mask__ (_mindspore.Tensor_)：attention_mask。

返回
* __out1__ (_mindspore.Tensor_): 输出。
* __out2__ (_mindspore.Tensor_): 输出。

> _class_ __mindtext.modules.encoder.luke.EntityAwareLayer__ _(config)_


> __init__ (_config)_)

参数 
* __config__ (_LukeConfig_): LukeConfig。

> __construct__ (_word_hidden_states, entity_hidden_states, attention_mask_)

参数
* __word_hidden_states__ (_mindspore.Tensor_): word的隐藏状态。
* __entity_hidden_states__ (_mindspore.Tensor_)：实体的隐藏状态。
* __attention_mask__ (_mindspore.Tensor_)：attention_mask。

返回
* __l_out1__ (_mindspore.Tensor_): 输出。
* __l_out2__ (_mindspore.Tensor_): 输出。

> _class_ __mindtext.modules.encoder.luke.EntityAwareEncoder__ _(config)_


> __init__ (_config_)

参数 
* __config__ (_LukeConfig_): LukeConfig。

> __construct__ (_word_hidden_states, entity_hidden_states, attention_mask_)

参数
* __word_hidden_states__ (_mindspore.Tensor_): word的隐藏状态。
* __entity_hidden_states__ (_mindspore.Tensor_)：实体的隐藏状态。
* __attention_mask__ (_mindspore.Tensor_)：attention_mask。

返回
* __word_hidden_states__ (_mindspore.Tensor_): word的隐藏状态。
* __entity_hidden_states__ (_mindspore.Tensor_)：实体的隐藏状态。

> _class_ __mindtext.modules.encoder.luke.BertOutput__ _(in_channels, out_channels, initializer_range=0.02, 
> dropout_prob=0.1, compute_type=mstype.float32)_


> __init__ (_in_channels, out_channels, initializer_range=0.02, dropout_prob=0.1, compute_type=mstype.float32_)

参数 
* __in_channels__ (_int_): 输入大小。
* __out_channels__ (_int_): 输出大小。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __dropout_prob__ (_float_): dropout的大小，默认为0.1。
* __compute_type__ (_mindspore.dtype_): Bert模型的计算数据类型，默认为mstype.float32。

> __construct__ (_hidden_status, input_tensor_)

参数
* __hidden_states__ (_mindspore.Tensor_)：隐藏状态。
* __input_tensor__ (_mindspore.Tensor_)：输入向量。

返回
* __out__ (_mindspore.Tensor_): 输出。

> _class_ __mindtext.modules.encoder.luke.BertEncoderCell__ _(hidden_size=1024,
                 seq_length=512,
                 num_attention_heads=16,
                 intermediate_size=3072,
                 attention_probs_dropout_prob=0.02,
                 use_one_hot_embeddings=False,
                 initializer_range=0.02,
                 hidden_dropout_prob=0.1,
                 use_relative_positions=False,
                 hidden_act="gelu",
                 compute_type=mstype.float32)_


> __init__ (_hidden_size=1024,
                 seq_length=512,
                 num_attention_heads=16,
                 intermediate_size=3072,
                 attention_probs_dropout_prob=0.02,
                 use_one_hot_embeddings=False,
                 initializer_range=0.02,
                 hidden_dropout_prob=0.1,
                 use_relative_positions=False,
                 hidden_act="gelu",
                 compute_type=mstype.float32_)

参数
* __hidden_size__ (_int_): bert编码层的大小，默认为1024。
* __seq_length__ (_int_): 输入的长度，默认为512。
* __num_attention_heads__ (_int_): 注意力头数量，默认为16。
* __intermediate_size__ (_int_): 中间层的隐藏维度大小，默认为3072。
* __attention_probs_dropout_prob__ (_float_): BertAttention的dropout概率，默认为0.02。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __use_relative_positions__ (_bool_): 是否使用关系位置，默认为False。
* __hidden_act__ (_str_): bert的激活函数，默认为“gelu”。
* __hidden_dropout_prob__ (_float): dropout的大小，默认为0.1。
* __compute_type__ (_mindspore.dtype_): Bert模型的计算数据类型，默认为mstype.float32。

> __construct__ (_hidden_states, attention_mask_)

参数
* __hidden_states__ (_mindspore.Tensor_): 隐藏状态。
* __attention_mask__ (_mindspore.Tensor_): attention_mask。

返回
* __output__ (_mindspore.Tensor_): encoder的输出。

> _class_ __mindtext.modules.encoder.luke.BertSelfOutput__ _(config, compute_type=mstype.float32)_


> __init__ (_config, compute_type=mstype.float32_)

参数 
* __config__ (_LukeConfig_): LukeConfig。
* __compute_type__ (_mindspore.dtype_): Bert模型的计算数据类型，默认为mstype.float32。

> __construct__ (_hidden_status, input_tensor_)

参数
* __hidden_states__ (_mindspore.Tensor_)：隐藏状态。
* __input_tensor__ (_mindspore.Tensor_)：输入向量。

> _class_ __mindtext.modules.encoder.luke.BertSelfAttention__ _(seq_length,
                 hidden_size=768,
                 num_attention_heads=12,
                 attention_probs_dropout_prob=0.1,
                 use_one_hot_embeddings=False,
                 initializer_range=0.02,
                 hidden_dropout_prob=0.1,
                 use_relative_positions=False,
                 compute_type=mstype.float32)_


> __init__ (_seq_length,
                 hidden_size=768,
                 num_attention_heads=12,
                 attention_probs_dropout_prob=0.1,
                 use_one_hot_embeddings=False,
                 initializer_range=0.02,
                 hidden_dropout_prob=0.1,
                 use_relative_positions=False,
                 compute_type=mstype.float32_)

参数
* __seq_length__ (_int_): 输入的长度。
* __hidden_size__ (_int_): bert编码层的大小，默认为768。
* __num_attention_heads__ (_int_): 注意力头数量，默认为12。
* __attention_probs_dropout_prob__ (_float_): BertAttention的dropout概率，默认为0.1。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __use_relative_positions__ (_bool_): 是否使用关系位置，默认为False。
* __hidden_dropout_prob__ (_float): dropout的大小，默认为0.1。
* __compute_type__ (_mindspore.dtype_): Bert模型的计算数据类型，默认为mstype.float32。

> __construct__ (_input_tensor, attention_mask_)

参数
* __input_tenso__ (_mindspore.Tensor_): 输入向量。
* __attention_mask__ (_mindspore.Tensor_): attention_mask。

返回
* __output__ (_mindspore.Tensor_): encoder的输出。

> _class_ __mindtext.modules.encoder.luke.BertAttention__ _(from_tensor_width,
                 to_tensor_width,
                 from_seq_length,
                 to_seq_length,
                 num_attention_heads=1,
                 size_per_head=512,
                 query_act=None,
                 key_act=None,
                 value_act=None,
                 has_attention_mask=False,
                 attention_probs_dropout_prob=0.0,
                 use_one_hot_embeddings=False,
                 initializer_range=0.02,
                 do_return_2d_tensor=False,
                 use_relative_positions=False,
                 compute_type=mstype.float32)_

使用多头治理力机制

> __init__ (_from_tensor_width,
                 to_tensor_width,
                 from_seq_length,
                 to_seq_length,
                 num_attention_heads=1,
                 size_per_head=512,
                 query_act=None,
                 key_act=None,
                 value_act=None,
                 has_attention_mask=False,
                 attention_probs_dropout_prob=0.0,
                 use_one_hot_embeddings=False,
                 initializer_range=0.02,
                 do_return_2d_tensor=False,
                 use_relative_positions=False,
                 compute_type=mstype.float32_)

参数
* __from_tensor_width__ (_int_): from_tensor的最后一维大小。
* __to_tensor_width__ (_int_): to_tensor的最后一维大小。
* __from_seq_length__ (_int_): from_tensor的序列长度。
* __to_seq_length__ (_int_): to_tensor的序列长度。
* __num_attention_heads__ (_int_): 注意力头数量，默认为1。
* __size_per_head__ (_int_): 每个注意力头的维度大小，默认为512。
* __query_act__ (_str_): query transformer的激活函数，默认为None。
* __key_act__ (_str_): key transformer的激活函数，默认为None。
* __value_act__ (_str_): value transformer的激活函数，默认为None。
* __out_act__ (_str_): output transformer的激活函数，默认为None。
* __has_attention_mask__ (_bool_): 是否使用attention mask，默认为False。
* __attention_probs_dropout_prob__ (_float_): 自注意力的dropout，默认为0.0。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __do_return_2d_tensor__ (_bool_): True返回2维张量，False返回3维张量，默认为False。
* __use_relative_positions__ (_bool_): 是否使用关系位置，默认为False。
* __computer_type__ (_mindspore.dtype_): 注意力计算的类型，默认为mstype.float32。

> __construct__ (_from_tensor, to_tensor, attention_mask_)

参数
* __from_tensor__ (_mindspore.Tensor_)：from_tensor，通常是一个attention的query向量(Q)，shape是(batch_size, from_seq_len, dim)。
* __to_tensor__ (_mindspore.Tensor_)：to_tensor，通常是key和value对于attention来说K=V，shape是(batch_size, to_seq_len, dim)。
* __attention_mask__ (_Optional[mindspore.Tensor]_)：注意力的mask矩阵（2D或者3D），值是[0/1]或者[True/False]，默认为None，shape为(from_seq_len, to_seq_len)或者(batch_size, from_seq_len, to_seq_len)。

返回
* __context_layer__ (_mindspore.Tensor_): 注意力层的输出。

> _class_ __mindtext.modules.encoder.luke.SaturateCast__ _(dst_type=mstype.float32)_

执行安全的转换

> __init__ (_dst_type=mstype.float32)

参数
* __dst_type__ (_int_): 输出向量的类型，默认为mstype.float32。

> __construct__ (_x_)

返回
* __x__ (_mindspore.Tensor_): 转换后的输出。

> _class_ __mindtext.modules.encoder.luke.RelaPosEmbeddingsGenerator__ _(length, depth, max_relative_position, 
> initializer_range, use_one_hot_embeddings=False)_

生成size为[length, length, depth]的向量

> __init__ (_length, depth, max_relative_position, initializer_range, use_one_hot_embeddings=False)

参数
* __length__ (_int_): 生成的矩阵一维的长度。
* __depth__ (_int_): 注意力头的大小。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __max_relative_position__ (_int_): 相关位置的最大值。

> __construct__ ()

返回
* __embeddings__ (_mindspore.Tensor_): 输出。

> _class_ __mindtext.modules.encoder.luke.RelaPosMatrixGenerator__ _(length, max_relative_position)_

生成输入直接的相关位置矩阵

> __init__ (_length, max_relative_position)

参数
* __length__ (_int_): 生成的矩阵一维的长度。
* __max_relative_position__ (_int_): 相关位置的最大值。

> __construct__ ()

返回
* __final_mat__ (_mindspore.Tensor_): 输出。