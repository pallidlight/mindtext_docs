# mindtext.modules.encoder.bart

> _class_ __mindtext.modules.encoder.bart.BartConfig__ _(max_sent_len=5, vocab_size=50265,
                 max_position_embeddings=1024,
                 encoder_layers=12,
                 encoder_ffn_dim=4096,
                 encoder_attention_heads=16,
                 decoder_layers=12,
                 decoder_ffn_dim=4096,
                 decoder_attention_heads=16,
                 encoder_layerdrop=1.0,
                 decoder_layerdrop=1.0,
                 activation_function="gelu",
                 d_model=1024,
                 dropout=0.9,
                 attention_dropout=1.0,
                 activation_dropout=1.0,
                 init_std=0.02,
                 classifier_dropout=1.0,
                 scale_embedding=False,
                 gradient_checkpointing=False,
                 use_cache=True,
                 num_labels=3,
                 pad_token_id=1,
                 bos_token_id=0,
                 eos_token_id=2,
                 is_encoder_decoder=True,
                 decoder_start_token_id=2,
                 forced_eos_token_id=2)_

这是一个配置类用来保存模型的配置信息，用来初始化BART模型，定义模型结构。

> __init__ (_max_sent_len=5, vocab_size=50265,
                 max_position_embeddings=1024,
                 encoder_layers=12,
                 encoder_ffn_dim=4096,
                 encoder_attention_heads=16,
                 decoder_layers=12,
                 decoder_ffn_dim=4096,
                 decoder_attention_heads=16,
                 encoder_layerdrop=1.0,
                 decoder_layerdrop=1.0,
                 activation_function="gelu",
                 d_model=1024,
                 dropout=0.9,
                 attention_dropout=1.0,
                 activation_dropout=1.0,
                 init_std=0.02,
                 classifier_dropout=1.0,
                 scale_embedding=False,
                 gradient_checkpointing=False,
                 use_cache=True,
                 num_labels=3,
                 pad_token_id=1,
                 bos_token_id=0,
                 eos_token_id=2,
                 is_encoder_decoder=True,
                 decoder_start_token_id=2,
                 forced_eos_token_id=2_)

参数
* __vocab_size_ (_int_): BART模型的词典大小，默认为50265。
* __d_model__ (_int_): 层和池化层的维度，默认为1024。
* __encoder_layers__ (_int_): 编码器的数量，默认为12。
* __decoder_layers__ (_int_): 解码器的数量，默认为12。
* __encoder_attention_heads__ (_int_): 在Transformer编码器里面每一个attention层的注意力头数量，默认为16。
* __decoder_attention_heads__ (_int_): 在Transformer解码器里面每一个attention层的注意力头数量，默认为16。
* __decoder_ffn_dim__ (_int_): 解码器中间层的维度，默认为4096。
* __encoder_ffn_dim__ (_int_): 编码器中间层的维度，默认为4096。
* __activation_function__ (_str_): 激活函数。
* __dropout__ (_float_): 全部在embeddings，encoder，pooler的全连接层dropout的大小，默认为0.1。
* __attention_dropout__ (_float_): attention的dropout大小，默认为0.0。
* __activation_dropout__ (_float_): 全连接层内部的激活函数的dropout，默认为0.0。
* __classifier_dropout__ (_float_): 分类器的dropout大小，默认为0.0。
* __max_position_embeddings__ (_int_): 模型可用的最大序列长度，建议设置较大数值(e.g., 512 or 1024 or 2048)，默认为1024。
* __init_std__ (_float_): 初始化权重矩阵所用的截断正态分布的标准差，默认为0.02。
* __encoder_layerdrop__ (_float_): 编码层的drop概率，默认为0.0，详情见<https://arxiv.org/abs/1909.11556>。
* __decoder_layerdrop__ (_float_): 解码层的drop概率，默认为0.0，详情见<https://arxiv.org/abs/1909.11556>。
* __gradient_checkpointing__ (_bool_): 如果为真，使用梯度保存来保存expense of slower backward pass，默认为False。
* __scale_embedding__ (_bool_): Scale embeddings by diving by sqrt(d_model)，默认为False。
* __use_cache__ (_float_): 是否模型返回最行的key/value attention，默认为True。
* __num_labels__ (_int_): 在class:transformers.BartForSequenceClassification中labels的数量，默认为3。
* __forced_eos_token_id__ (_int_): 当达到最大长度，token的id被强制为最后一个token。默认为2。

> __shift_tokens_right__ (_input_ids: mindspore.Tensor, pad_token_id: int, decoder_start_token_id: int_)

将输入的ids转移到右边

参数 
* __input_ids__ (_mindspore.Tensor_): 输入的ids。
* __pad_token_id__ (_int_): 用来填充的token id。

返回
* __shifted_input_ids__ (_mindspore.Tensor_)：返回的向量。

> _class_ __mindtext.modules.encoder.bart.BartLearnedPositionalEmbedding__ _(num_embeddings: int, embedding_dim: int)_

通过被修整后的最大size学习位置编码

> __init__ (_num_embeddings: int, embedding_dim: int_)

参数 
* __num_embeddings__ (_int_): embedding的数量。
* __embedding_dim__ (_int_): embedding的维度。

> __construct__ (_input_ids_shape: mindspore.Tensor.shape, past_key_values_length: int = 0_)

参数

返回

> _class_ __mindtext.modules.encoder.bart.BartAttention__ _(embed_dim: int, num_heads: int, dropout: float = 0.0, 
> is_decoder: bool = False, bias: bool = True)_

多头注意力机制

> __init__ (_embed_dim: int, num_heads: int, dropout: float = 0.0, is_decoder: bool = False, bias: bool = True_)

参数 
* __embed_dim__ (_int_): embedding的维度。
* __num_heads__ (_int_): 注意力头的数量。
* __dropout__ (_float_): dropout的大小，默认为0.0。
* __bias__ (_bool_): 是否使用偏置，默认为True。

> __construct__ (_hidden_states: mindspore.Tensor, key_value_states: Optional[mindspore.Tensor] = None, 
> past_key_value: Optional[Tuple[mindspore.Tensor]] = None, attention_mask: Optional[mindspore.Tensor] = None_)

参数
* __hidden_states__ (_mindspore.Tensor_): 
* __key_value_states__ (_ Optional[mindspore.Tensor]_): 
* __past_key_value__ (_Optional[Tuple[mindspore.Tensor]]_):
* __embed_dim__ (_Optional[mindspore.Tensor]_): 

返回
* __attn_output__ (_mindspore.Tensor_): embedding的维度。
* __past_key_value__ (_ Optional[mindspore.Tensor]_): 缓存过去key and value projection的隐藏状态。

> _class_ __mindtext.modules.encoder.bart.BartEncoderLayer__ _(config: BartConfig)_

编码层

> __init__ (_config: BartConfig_)

参数 
* __config__ (_BartConfig_): BART模型的配置。

> __construct__ (_hidden_states: mindspore.Tensor, attention_mask: mindspore.Tensor_)

参数
* __hidden_states__ (_mindspore.Tensor_): 输入向量，shape为(seq_len, batch, embed_dim)。
* __attention_mask__ (_ Optional[mindspore.Tensor]_): attention mask的size(batch, 1, tgt_len, src_len)，其中填充元素通过非常大的负数来表明。

返回
* __outputs__ (_mindspore.Tensor_): 

> _class_ __mindtext.modules.encoder.bart.BartDecoderLayer__ _(config: BartConfig)_

解码层

> __init__ (_config: BartConfig_)

参数 
* __config__ (_BartConfig_): BART模型的配置。

> __construct__ (_hidden_states: mindspore.Tensor, attention_mask: Optional[mindspore.Tensor] = None, 
> encoder_hidden_states: Optional[mindspore.Tensor] = None, encoder_attention_mask: Optional[mindspore.Tensor] = None, 
> past_key_value: Optional[Tuple[mindspore.Tensor]] = None_)

参数
* __hidden_states__ (_mindspore.Tensor_): 输入向量，shape为(seq_len, batch, embed_dim)。
* __attention_mask__ (_ Optional[mindspore.Tensor]_): attention mask的size(batch, 1, tgt_len, src_len)，其中填充元素通过非常大的负数来表明。
* __encoder_hidden_states__ (_mindspore.Tensor_): 通过attention到该层的shape(seq_len, batch, embed_dim)。
* __encoder_hidden_mask__ (_mindspore.Tensor_): attention mask的size(batch, 1, tgt_len, src_len)，其中填充元素通过非常大的负数来表明。
* __past_key_value__ (_mindspore.Tensor_): 缓存过去key and value projection的隐藏状态。

返回
* __outputs__ (_mindspore.Tensor_): 

> _class_ __mindtext.modules.encoder.bart.BartClassificationHead__ _(input_dim: int, inner_dim: int, num_classes: int, 
> pooler_dropout: float)_

句子级别分类任务的头

> __init__ (_input_dim: int, inner_dim: int, num_classes: int, pooler_dropout: float_)

参数 
* __input_dim__ (_int_): 输入的维度。
* __inner_dim__ (_int_):
* __num_classes__ (_int_): 分类的数量。
* __pooler_dropout__ (_float_): pooler的dropout的大小。

> __construct__ (_hidden_states: mindspore.Tensor_)

参数
* __hidden_states__ (_mindspore.Tensor_): 

返回
* __hidden_states__ (_mindspore.Tensor_): 

> _class_ __mindtext.modules.encoder.bart.BartPretrainedModel__ _(module)_

BART预训练模型

> _class_ __mindtext.modules.encoder.bart.BartEncoder__ _(config: BartConfig, embed_tokens: Optional[nn.Embedding] = None)_

Transformer的encoder包含*config.encoder_layers* self attention layers，每一层都是class:`BartEncoderLayer

> __init__ (_config: BartConfig, embed_tokens: Optional[nn.Embedding] = None_)

参数 
* __config__ (_BartConfig_): BartConfig。
* __embed_tokens__ (_mindspore.nn.Embedding_):输出的embedding

> __construct__ (_input_ids=None, attention_mask=None_)

参数
* __input_ids__ (_mindspore.Tensor_): 输入在字典里面的索引，填充会被默认忽略，需要提供填充。
* __attention_mask__ (_mindspore.Tensor_): attention mask，值是0/1

返回
* __hidden_states__ (_mindspore.Tensor_): 

> _class_ __mindtext.modules.encoder.bart.BartDecoder__ _(config: BartConfig, embed_tokens: Optional[nn.Embedding] = None)_

Transformer的dncoder包含*config.encoder_layers* self attention layers，每一层都是class:`BartEncoderLayer

> __init__ (_config: BartConfig, embed_tokens: Optional[nn.Embedding] = None_)

参数 
* __config__ (_BartConfig_): BartConfig。
* __embed_tokens__ (_mindspore.nn.Embedding_):输出的embedding

> __construct__ (_input_ids=None, attention_mask=None, encoder_hidden_states=None, encoder_attention_mask=None, past_key_values=None_)

参数
* __input_ids__ (_mindspore.Tensor_): 输入在字典里面的索引，填充会被默认忽略，需要提供填充。。
* __attention_mask__ (_ Optional[mindspore.Tensor]_): attention mask，值是0/1。
* __encoder_hidden_states__ (_mindspore.Tensor_): 编码层的最后一层的输出隐藏状态。
* __encoder_hidden_mask__ (_mindspore.Tensor_): attention mask，值是0/1。
* __past_key_value__ (_mindspore.Tensor_): 包含注意块的预计算键和值隐藏状态。可以用来加速 解码。

返回
* __hidden_states__ (_mindspore.Tensor_): 

> _class_ __mindtext.modules.encoder.bart.BartModel__ _(config: BartConfig)_

BART模型

> __init__ (_config: BartConfig_)

参数 
* __config__ (_BartConfig_): BartConfig。

> __construct__ (_input_ids=None, attention_mask=None, decoder_input_ids=None, decoder_attention_mask=None, past_key_values=None_)

参数
* __input_ids__ (_mindspore.Tensor_): 输入在字典里面的索引，填充会被默认忽略，需要提供填充。。
* __attention_mask__ (_ Optional[mindspore.Tensor]_): attention mask，值是0/1。
* __decoder_input_ids__ (_mindspore.Tensor_): 
* __decoder_attention_mask__ (_mindspore.Tensor_): attention mask，值是0/1。
* __past_key_value__ (_mindspore.Tensor_): 包含注意块的预计算键和值隐藏状态。可以用来加速 解码。

返回
* __hidden_states__ (_mindspore.Tensor_): 

> _class_ __mindtext.modules.encoder.bart.BartForConditionalGeneration__ _(config: BartConfig)_

BART模型

> __init__ (_config: BartConfig_)

参数 
* __config__ (_BartConfig_): BartConfig。

> __construct__ (_input_ids=None, attention_mask=None, labels=None, decoder_attention_mask=None, past_key_values=None_)

参数
* __input_ids__ (_mindspore.Tensor_): 输入在字典里面的索引，填充会被默认忽略，需要提供填充。。
* __attention_mask__ (_ Optional[mindspore.Tensor]_): attention mask，值是0/1。
* __labels__ (_mindspore.Tensor_): 语言掩盖模型计算loss的标签。
* __decoder_attention_mask__ (_mindspore.Tensor_): attention mask，值是0/1。
* __past_key_value__ (_mindspore.Tensor_): 包含注意块的预计算键和值隐藏状态。可以用来加速 解码。

返回

> _class_ __mindtext.modules.encoder.bart.BartForConditionalGenerationOneStep__ _(net, optimizer, sens=1.0)_

BartForConditionalGenerationOneStep

> __init__ (_net, optimizer, sens=1.0_)

参数 
* __net__ (_BartConfig_): BartConfig。
* __optimizer__ (_BartConfig_): BartConfig。
* __sens=1.0__ (_BartConfig_): BartConfig。

> __construct__ (_nput_ids, attention_mask=None, labels=None_)

参数
* __input_ids__ (_mindspore.Tensor_): 输入在字典里面的索引，填充会被默认忽略，需要提供填充。。
* __attention_mask__ (_ Optional[mindspore.Tensor]_): attention mask，值是0/1。
* __labels__ (_mindspore.Tensor_): 语言掩盖模型计算loss的标签。


返回



