# mindtext.modules.encoder.pool

> _class_ __mindtext.modules.encoder.pool.MaxPool__ _(kernel_size: Optional[int] = None, stride: int = 1, dimension: int = 1, 
> pad_mode: str = "valid")_

最大池化模型

> __init__ (_kernel_size: Optional[int] = None, stride: int = 1, dimension: int = 1, pad_mode: str = "valid"_)

参数
* __kernel_size__ (_Optional[int]_): 卷积核的大小，默认为tensor.shape[-1]。
* __stride__ (_int_): 最大池化的步长，默认为1。
* __dimension__ (_int_): 最大池化的维度，支持dimension[1,2]，默认为1。
* __pad_mode__ (_str_): 1.same 2.valid，默认为"valid。

> __construct__ (_x: mindspore.Tensor_)
 
参数
* __x__ (_mindspore.Tensor_): 输入的向量，shape为(N, L, C)。

返回
* __x__ (_mindspore.Tensor_): 返回的向量，shape为(N, C)。

> _class_ __mindtext.modules.encoder.pool.MaxPoolWithMask__ _()_

最大池化模型带有mask机制，不考虑mask为0

> __init__ ()

> __construct__ (_tensor: mindspore.Tensor, mask: mindspore.Tensor, axis: int = 1_)
 
参数
* __tensor__ (_mindspore.Tensor_): 输入的向量，shape为(batch_size, seq_len, channels)。
* __mask__ (_mindspore.Tensor_): mask向量值必须为0/1或者True/False，shape为(batch_size, seq_len)。
* __axis__ (_int_): 最大池化的维度，默认为1。

返回
* __tensor__ (_mindspore.Tensor_): 带有mask的最大池化返回。

> _class_ __mindtext.modules.encoder.pool.KMaxPool__ _(k: int = 1)_

K最大池化

> __init__ (_k: int = 1_)
 
参数
* __k__ (_int_): KMaxPool的k值。

> __construct__ (_x: mindspore.Tensor_)
 
参数
* __x__ (_mindspore.Tensor_): 输入的向量，shape为(N, L, C)。

返回
* __x__ (_mindspore.Tensor_): 输出向量，shape为(N, C*k)。

> _class_ __mindtext.modules.encoder.pool.AvgPool__ _(stride: int = 1, dimension: int = 1, pad_mode: str = "valid")_

在最后一个维度的平均池化

> __init__ (_stride: int = 1, dimension: int = 1, pad_mode: str = "valid"_)
 
参数
* __stride__ (_int_): 步长。
* __dimension__ (_int_): 最大池化的维度，支持dimension[1,2]，默认为1。
* __pad_mode__ (_str_): 1.same 2.valid，默认为"valid。

> __construct__ (_x: mindspore.Tensor_)
 
参数
* __x__ (_mindspore.Tensor_): 输入的向量，shape为(N, L, C)。

返回
* __x__ (_mindspore.Tensor_): 输出向量，shape为(N, C)。

> _class_ __mindtext.modules.encoder.pool.AvgPoolWithMask__ _()_

带有mask的平均池化

> __init__ ()

> __construct__ (_x: mindspore.Tensor_)
 
参数
* __tensor__ (_mindspore.Tensor_): 输入的向量，shape为(batch_size, seq_len, channels)。
* __mask__ (_mindspore.Tensor_): mask向量值必须为0/1或者True/False，shape为(batch_size, seq_len)。
* __axis__ (_int_): 最大池化的维度，默认为1。

返回
* __x__ (_mindspore.Tensor_): 输出向量。