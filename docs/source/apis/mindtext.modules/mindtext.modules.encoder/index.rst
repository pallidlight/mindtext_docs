mindtext.embeddings
============

.. automodule:: mindtext.modules.encoder

子模块
------

.. toctree::
   :maxdepth: 1

   mindtext.modules.encoder.attention
   mindtext.modules.encoder.bart
   mindtext.modules.encoder.bert
   mindtext.modules.encoder.conv
   mindtext.modules.encoder.conv_maxpool
   mindtext.modules.encoder.luke
   mindtext.modules.encoder.pool
   mindtext.modules.encoder.roberta
   mindtext.modules.encoder.seq2seq_encoder
   mindtext.modules.encoder.xlnet

