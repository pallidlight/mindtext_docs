# mindtext.modules.encoder.xlnet

> _class_ __mindtext.modules.encoder.xlnet.XLNetConfig__ _(vocab_size: int = 32000,
            d_model: int = 1024,
            n_layer: int = 24,
            n_head: int = 16,
            d_inner: int = 4096,
            ff_activation: str = "gelu",
            untie_r: bool = True,
            attn_type_bi: bool = True,
            initializer_range: float = 0.02,
            layer_norm_eps: float = 1e-12,
            dropout: float = 0.1,
            mem_len: int = 512,
            reuse_len: Optional[int] = None,
            use_mems: bool = False,
            bi_data: bool = False,
            clamp_len: int = -1,
            same_length: bool = False,
            pad_token_id: int = 5,
            bos_token_id: int = 1,
            eos_token_id: int = 2,
            **kwargs)_

XLNetModel的配置信息，可通过json文件传入

Example
```
>>> xlconfig = XLNetConfig.from_json_file('json_file_path')
```

> __init__ (_vocab_size: int = 32000,
            d_model: int = 1024,
            n_layer: int = 24,
            n_head: int = 16,
            d_inner: int = 4096,
            ff_activation: str = "gelu",
            untie_r: bool = True,
            attn_type_bi: bool = True,
            initializer_range: float = 0.02,
            layer_norm_eps: float = 1e-12,
            dropout: float = 0.1,
            mem_len: int = 512,
            reuse_len: Optional[int] = None,
            use_mems: bool = False,
            bi_data: bool = False,
            clamp_len: int = -1,
            same_length: bool = False,
            pad_token_id: int = 5,
            bos_token_id: int = 1,
            eos_token_id: int = 2,
            **kwargs_)

参数
* __vocab_size__ (_int_): xlnet model的字典大小，定义可以表示多少不同的tokens，默认为32000。
* __d_model__ (_int_): 编码层和pooler层的维度，默认为1024。
* __n_layer__ (_int_): Transformer解码器里面的编码层数量，默认为24。
* __n_head__ (_int_): 注意力层里面的注意力头数量，默认为16。
* __d_inner__ (_int_): Transformer里面前馈层的维度，默认为4096。
* __ff_activation__ (_str_): 非线性激活函数，默认为“gelu”。
* __untie_r__ (_bool_): 是否结合关系位置偏置，默认为True。
* __attn_type_bi__ (_bool_): 在模型里面使用的attention类型，默认为bi。
* __initializer_range__ (_float_): 截断正态分布的标准差，默认为0.02。
* __layer_norm_eps__ (_loat_): 在normalization层使用的epsilon，默认为1e-12。
* __dropout__ (_float_): 在embedding，编码和pooler层使用的dropout的大小，默认为0.1。
* __mem_len__ (_int_): 缓存的token数量，key/valuede在先前的前向传播计算过不会再计算，默认为512。
* __reuse_len__ (_Optional[int]_): 当前batch缓存的将被用于后面步骤的token数量，默认为None。
* __use_mems__ (_bool_): 是否模型使用记忆机制，默认为Falase。
* __bi_data__ (_bool_): 是否使用双向的输入流水线。通常在预训练设置为True，在微调上设置为False。
* __clamp_len__ (_int_): 夹紧所有大于夹紧长度的相对距离。将此属性设置为-1意味着无夹紧，默认为-1。
* __same_length__ (_bool_): 是否使用一些注意力长度，默认为False。
* __pad_token_id__ (_int_): 填充的token，默认为5。
* __bos_token_id__ (_int_): 输入开始的token，默认为1。
* __eos_token_id__ (_int_): 输入结束的token，默认为2。

> _class_ __mindtext.modules.encoder.xlnet.XLNetRelativeAttention__ _(config: XLNetConfig)_

Example
```
>>> xlconfig = XLNetConfig.from_json_file('json_file_path')
>>> xl_rel_attn = XLNetRelativeAttention(xlconfig)
```

> __init__ (_config: XLNetConfig_) 

参数
* __config__ (_XLNetConfig_): XLNetConfig。

> __construct__ (_h: Tensor, g: Tensor, attn_mask_h: Tensor, attn_mask_g: Tensor, r: Tensor, seg_mat: Tensor, 
> mems: Optional[Tensor] = None, target_mapping: Optional[Tensor] = None_)

xlnet 关系注意力层的前向传播

参数
* __h__ (_mindspore.Tensor_): H hidden states。
* __g__ (_mindspore.Tensor_): G hidden states。
* __attn_mask_h__ (_mindspore.Tensor_): H hidden states的attention mask。
* __attn_mask_g__ (_mindspore.Tensor_): G hidden states的attention mask。
* __r__ (_mindspore.Tensor_): 位置编码。
* __seg_mat__ (_mindspore.Tensor_): segment编码。
* __mems__ (_Optional[Tensor]_): Mems向量。
* __target_mapping__ (_Optional[Tensor].Tensor_): xlnet target映射。

返回
* __output_h__ (_Union[Tensor, Tuple[Tensor]]_): 关系注意力。

> _class_ __mindtext.modules.encoder.xlnet.XLNetFeedForward__ _(config: XLNetConfig)_

Example
```
>>> xlconfig = XLNetConfig.from_json_file('json_file_path')
>>> xl_feed_forward = XLNetFeedForward(xlconfig)
```

> __init__ (_config: XLNetConfig_) 

参数
* __config__ (_XLNetConfig_): XLNetConfig。

> __construct__ (_inp: Tensor_)

参数
* __inp__ (_mindspore.Tensor_): 输入向量。


返回
* __output__ (_mindspore.Tenso_): 输出向量。

> _class_ __mindtext.modules.encoder.xlnet.XLNetLayer__ _(config: XLNetConfig)_

XLNet 编码层

Example
```
>>> xlconfig = XLNetConfig.from_json_file('json_file_path')
>>> xl_layer = XLNetLayer(xlconfig)
```

> __init__ (_config: XLNetConfig_) 

参数
* __config__ (_XLNetConfig_): XLNetConfig。

> __construct__ (_h: Tensor, g: Tensor, attn_mask_h: Tensor, attn_mask_g: Tensor, r: Tensor, seg_mat: Tensor, 
> mems: Optional[Tensor] = None, target_mapping: Optional[Tensor] = None_)

参数
* __h__ (_mindspore.Tensor_): H hidden states。
* __g__ (_mindspore.Tensor_): G hidden states。
* __attn_mask_h__ (_mindspore.Tensor_): H hidden states的attention mask。
* __attn_mask_g__ (_mindspore.Tensor_): G hidden states的attention mask。
* __r__ (_mindspore.Tensor_): 位置编码。
* __seg_mat__ (_mindspore.Tensor_): segment编码。
* __mems__ (_Optional[Tensor]_): Mems向量。
* __target_mapping__ (_Optional[Tensor].Tensor_): xlnet target映射。


返回
* __output_h__ (_Union[Tensor, Tuple[Tensor]]_): xlnet编码注意力。

> _class_ __mindtext.modules.encoder.xlnet.XLNetModel__ _(config: XLNetConfig)_

XLNet模型

Example
```
>>> xlconfig = XLNetConfig.from_json_file('json_file_path')
>>> xlnet = XLNetModel(xlconfig)
```

> __init__ (_config: XLNetConfig_) 

参数
* __config__ (_XLNetConfig_): XLNetConfig。

> __construct__ (_input_ids: Tensor, attention_mask: Tensor, token_type_ids: Tensor, mems: Optional[List[Tensor]] = None, 
> perm_mask: Optional[Tensor] = None, target_mapping: Optional[Tensor] = None_)

参数
* __input_ids__ (_mindspore.Tensor_): 输入的index序列。
* __attention_mask__ (_mindspore.Tensor_): 输入的注意力掩码。
* __token_type_ids__ (_mindspore.Tensor_): 输入序列类型。
* __mems__ (_Optional[Tensor]_): Mems向量。
* __perm_mask__ (_Optional[Tensor]_): Perm mask。
* __target_mapping__ (_Optional[Tensor].Tensor_): target映射。


返回
* __output__ (_Union[Tensor, Tuple[Tensor]]_): 输出。

> _class_ __mindtext.modules.encoder.xlnet.XLNetFinetuneCell__ _(network: nn.Cell, optimizer: nn.Optimizer, 
> scale_update_cell: Optional[nn.Cell] = None)_

XLNet微调模块

> __init__ (_network: nn.Cell, optimizer: nn.Optimizer, scale_update_cell: Optional[nn.Cell] = None_) 

参数
* __network__ (_nn.Cell_): XLNet模型，例如XLNetForClassification。
* __optimizer__ (_nn.Optimizer_): 优化器。
* __scale_update_cell__ (_Optional[nn.Cell]_): Scaling loss。

> __construct__ (_input_ids: Tensor, token_type_id: Tensor, attention_mask: Tensor, label: Tensor, sens: Optional[int] = None_)

参数
* __input_ids__ (_mindspore.Tensor_): 输入的index序列。
* __attention_mask__ (_mindspore.Tensor_): 输入的注意力掩码。
* __token_type_ids__ (_mindspore.Tensor_): 输入序列类型。
* __label__ (_mindspore.Tensor_): 标签索引。
* __sens__ (_Optional[Tensor]_): 梯度对输出的灵敏度。


返回
* __loss, cond__ (_Tuple[Tensor, Tensor]_): loss。

> _class_ __mindtext.modules.encoder.xlnet.XLNetForClassification__ _(model: XLNetModel, config: XLNetConfig, num_class: int = 2, 
> loss: Optional[nn.Cell] = None)_

XLNet的分类微调模块

> __init__ (_model: XLNetModel, config: XLNetConfig, num_class: int = 2, loss: Optional[nn.Cell] = None_) 

参数
* __model__ (_nn.Cell_): XLNet模型。
* __config__ (_XLNetConfig_): XLNetConfig。
* __num_class__ (_int_): 分类的数量。
* __loss__ (_Optional[nn.Cell]_): 损失函数。

> __construct__ (_input_ids, attention_mask, token_type_ids, label: Optional[Tensor] = None_)

参数
* __input_ids__ (_mindspore.Tensor_): 输入的index序列。
* __attention_mask__ (_mindspore.Tensor_): 输入的注意力掩码。
* __token_type_ids__ (_mindspore.Tensor_): 输入序列类型。
* __label__ (_mindspore.Tensor_): 标签索引。


返回
* __loss__ (_Tuple[Tensor, Tensor]_): loss。