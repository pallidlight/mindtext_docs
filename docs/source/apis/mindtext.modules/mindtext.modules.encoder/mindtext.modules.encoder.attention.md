# mindtext.modules.encoder.attention

> _class_ __mindtext.modules.encoder.attention.CastWrapper__ _(src_type: mindspore.dtype = mstype.float32, 
> dst_type: mindspore.dtype = mstype.float32)_

> __init__ (_src_type: mindspore.dtype = mstype.float32,  dst_type: mindspore.dtype = mstype.float32_)

参数
* __src_type__ (_mindspore.dtype_): 输入的向量类型。
* __dst_type__ (_mindspore.dtype_): 转换输出的向量类型。

> __construct__ (_x: mindspore.Tensor_)

参数
* __x__ (_mindspore.Tensor_)：需要转换类型的向量。

返回
* __x__(_mindspore.Tensor_)：转换类型后的向量。

> _class_ __mindtext.modules.encoder.attention.LayerPreprocess__ _(in_channels: Optional[int] = None)_

每层的预处理

> __init__ (_in_channels: Optional[int] = None_)

参数
* __in_channels__ (_Optional[int]_): 输入的大小，通常是输入的最后一维。

> __construct__ (_input_tensor: mindspore.Tensor_)

参数
* __input_tensor__ (_mindspore.Tensor_)：预处理层的输入。

返回
* __output__(_mindspore.Tensor_)：预处理层的输出。

> _class_ __mindtext.modules.encoder.attention.LayerPostprocess__ _(dropout_prob: float = 0.1)_

每层的后处理

> __init__ (_dropout_prob: float = 0.1_)

参数
* __dropout_prob__ (_float_): 后处理层的dropout，默认为0.1。

> __construct__ (_hidden_tensor: mindspore.Tensor, input_tensor: mindspore.Tensor_)

参数
* __hidden_tensor__ (_mindspore.Tensor_)：隐藏层的输出。
* __input_tensor__ (_mindspore.Tensor_)：隐藏层的输入。

返回
* __output__(_mindspore.Tensor_)：后处理层的输出。

> _class_ __mindtext.modules.encoder.attention.DotAttention__ _(key_size: int, value_size: int, dropout: float = 0.0,
> has_attn_mask: bool = False)_

Transformer里的DoAttention

> __init__ (_key_size: int, value_size: int, dropout: float = 0.0, has_attn_mask: bool = False_)

参数
* __key_size__ (_int_): key的最后一维大小。
* __value_size__ (_int_): value的最后一维大小。
* __dropout__ (_float_): dropout，默认为0.0。
* __has_attn_mask__ (_bool_): 是否有attention mask， 默认为False。

> __construct__ (_q: mindspore.Tensor, k: mindspore.Tensor, v: mindspore.Tensor, attn_mask: Optional[mindspore.Tensor] = None_)

参数
* __q__ (_mindspore.Tensor_)：Attention的queries，shape为(batch_size, q_len, q_size)。
* __k__ (_mindspore.Tensor_)：Attention的keys，shape为(batch_size, q_len, q_size)。
* __v__ (_mindspore.Tensor_)：Attention的value，shape为(batch_size, q_len, q_size)。
* __attn_mask__ (_Optional[mindspore.Tensor]_)：Attention的mask矩阵，值为True或者False，默认为None，shape为(batch_size, q_len, q_len)。

返回
* __output__(_mindspore.Tensor_)：DoAttention的输出。

> _class_ __mindtext.modules.encoder.attention.MultiHeadAttention__ _(batch_size: int, from_tensor_width: int, to_tensor_width: int,
> out_tensor_width: int, num_attention_heads: int = 1, size_per_head: int = 512, query_act: Optional[str] = None, key_act: Optional[str] = None,
> value_act: Optional[str] = None, out_act: Optional[str] = None, has_attention_mask: bool = True, attention_probs_dropout_prob: float = 0.0,
> use_one_hot_embeddings: bool = False, initializer_range: float = 0.02, do_return_2d_tensor: bool = True,
> compute_type: mindspore.dtype = mstype.float32)_

多头注意力机制

> __init__ (_batch_size: int, from_tensor_width: int, to_tensor_width: int,
> out_tensor_width: int, num_attention_heads: int = 1, size_per_head: int = 512, query_act: Optional[str] = None, key_act: Optional[str] = None,
> value_act: Optional[str] = None, out_act: Optional[str] = None, has_attention_mask: bool = True, attention_probs_dropout_prob: float = 0.0,
> use_one_hot_embeddings: bool = False, initializer_range: float = 0.02, do_return_2d_tensor: bool = True,
> compute_type: mindspore.dtype = mstype.float32_)

参数
* __batch_size__ (_int_): 输入数据集的batch size。
* __from_tensor_width__ (_int_): from_tensor的最后一维大小。
* __to_tensor_width__ (_int_): to_tensor的最后一维大小。
* __num_attention_heads__ (_int_): 注意力头数量，默认为1。
* __size_per_head__ (_int_): 每个注意力头的维度大小，默认为512。
* __query_act__ (_str_): query transformer的激活函数，默认为None。
* __key_act__ (_str_): key transformer的激活函数，默认为None。
* __value_act__ (_str_): value transformer的激活函数，默认为None。
* __out_act__ (_str_): output transformer的激活函数，默认为None。
* __has_attention_mask__ (_bool_): 是否使用attention mask，默认为False。
* __attention_probs_dropout_prob__ (_float_): 自注意力的dropout，默认为0.1。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码模式，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __do_return_2d_tensor__ (_bool_): True返回2维张量，False返回3维张量，默认为False。
* __computer_type__ (_mindspore.dtype_): 注意力计算的类型，默认为mstype.float32。

> __construct__ (_from_tensor: mindspore.Tensor, to_tensor: mindspore.Tensor, seq_length: int, enc_seq_length: int, 
> attention_mask: Optional[mindspore.Tensor] = None_)

参数
* __from_tensor__ (_mindspore.Tensor_)：from_tensor，通常是一个attention的query向量(Q)，shape是(batch_size, from_seq_len, dim)。
* __to_tensor__ (_mindspore.Tensor_)：to_tensor，通常是key和value对于attention来说K=V，shape是(batch_size, to_seq_len, dim)。
* __seq_length__ (_int_)：from_tensor的长度。
* __enc_seq_length__ (_int_)：to_tensor的长度。
* __attn_mask__ (_Optional[mindspore.Tensor]_)：注意力的mask矩阵（2D或者3D），值是[0/1]或者[True/False]，默认为None，shape为(from_seq_len, to_seq_len)或者(batch_size, from_seq_len, to_seq_len)。

返回
* __output__(_mindspore.Tensor_)：多头注意力的输出。

> _class_ __mindtext.modules.encoder.attention.SelfAttention__ _(batch_size: int, hidden_size: int, num_attention_heads: int = 16,
> attention_probs_dropout_prob: float = 0.1, use_one_hot_embeddings: bool = False, initializer_range: float = 0.02, hidden_dropout_prob: float = 0.1, 
> has_attention_mask: bool = True, is_encdec_att: bool = False, compute_type: mindspore.dtype = mstype.float32)_

自注意力机制

> __init__ (_batch_size: int, hidden_size: int, num_attention_heads: int = 16,
> attention_probs_dropout_prob: float = 0.1, use_one_hot_embeddings: bool = False, initializer_range: float = 0.02, hidden_dropout_prob: float = 0.1, 
> has_attention_mask: bool = True, is_encdec_att: bool = False, compute_type: mindspore.dtype = mstype.float32_)

参数
* __batch_size__ (_int_): 输入数据集的batch size。
* __hidden_size__ (_int_): 注意力层的大小。
* __num_attention_heads__ (_int_): 注意力头的数量，默认为16。
* __attention_probs_dropout_prob__ (_float_): 自注意力的dropout，默认为0.1。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码模式，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __hidden_dropout_prob__ (_float_): 输出的dropout，默认为0.1。
* __has_attention_mask__ (_bool_): 是否使用attention mask，默认为False。
* __is_encdec_att__ (_bool_): 是否query序列和memory序列是不一样的，默认为False。
* __computer_type__ (_mindspore.dtype_): 注意力计算的类型，默认为mstype.float32。

> __construct__ (_input_tensor: mindspore.Tensor, memory_tensor: mindspore.Tensor, attention_mask: mindspore.Tensor, seq_length: int, 
> enc_seq_length: int_)

参数
* __input_tensor__ (_mindspore.Tensor_)：输入序列通常是自注意力的query向量(Q)，shape是(batch_size, seq_len, hidden_units)。
* __memory_tensor__ (_mindspore.Tensor_)：输入序列通常是key和value对于attention来说K=V，shape是 (batch_size, seq_len, hidden_units)。
* __attention_mask__ (_mindspore.Tensor_)：自注意力隐藏状态的mask矩阵（2D或者3D），值是[0/1]或者[True/False]，shape为(from_seq_len, to_seq_len)或者(batch_size, from_seq_len, to_seq_len)。
* __seq_length__ (_int_)：输入的长度。
* __ enc_seq_length__ (_int_)：memory_tensor的长度。

返回
* __output__(_mindspore.Tensor_)：自注意力的输出。