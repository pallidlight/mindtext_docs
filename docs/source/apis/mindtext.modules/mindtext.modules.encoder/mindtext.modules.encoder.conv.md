# mindtext.modules.encoder.conv

> _class_ __mindtext.modules.encoder.conv.ConvEncoder__ _(init_embed: StaticEmbedding, num_filters: int = 256, 
> kernel_size: int = 3, num_layers: int = 7, embed_dropout: float = 0.1)_

卷积编码器

Example
```
>>> vocab = Vocabulary()
>>> vocab.update(["i", "am", "fine"])
>>> embed = StaticEmbedding(vocab, model_dir_or_name=None, embedding_dim=100)
>>> conv_encoder = ConvEncoder(embed)
>>> words = mindspore.Tensor(np.random.randint(0, 3, (1, 256)))
>>> x = conv_encoder(words)
```

> __init__ (_init_embed: StaticEmbedding, num_filters: int = 256, kernel_size: int = 3, num_layers: int = 7, embed_dropout: float = 0.1_)

参数
* __init_embed__ (_StaticEmbedding_): StaticEmbedding。
* __num_filters__ (_int_): 过滤器的数量，默认为256。
* __kernel_size__ (_int_): 卷积核的大小，默认为3。
* __num_layers__ (_int_): CNN的复合数量，默认为7。
* __embed_dropout__ (_float_): dropout层的概率，默认为0.1。

> __construct__ (_word_)
 
参数
* __word__ (_Tensor_): 输入的向量。

返回
* __x__ (_Tensor_): 卷积编码层返回的向量。