# mindtext.modules.decoder.seq2seq_decoder

> _class_ __mindtext.modules.decoder.seq2seq_decoder.DecoderCell__ _(batch_size: int, hidden_size: int = 1024, num_attention_heads: int = 12,
> intermediate_size: int = 4096, attention_probs_dropout_prob: float = 0.02, use_one_hot_embeddings: bool = False, 
> initializer_range: float = 0.02, hidden_dropout_prob: float = 0.1, hidden_act: str = "relu", compute_type: mindspore.dtype = mstype.float32)_

Transformer 里使用的解码器模块。

Example
```
>>> 
```

> **init** (_batch_size: int, hidden_size: int = 1024, num_attention_heads: int = 12, intermediate_size: int = 4096,
> attention_probs_dropout_prob: float = 0.02, use_one_hot_embeddings: bool = False, initializer_range: float = 0.02,
> hidden_dropout_prob: float = 0.1, hidden_act: str = "relu", compute_type: mindspore.dtype = mstype.float32_)

参数
* __batch_size__ (_int_): 输入数据集的batch size。
* __hidden_size__ (_int_): Transformer隐藏层大小，默认为1024。
* __num_attention_heads__ (_int_): 注意力头的数量，默认为12。
* __intermediate_size__ (_int_): 中间层的大小，默认为4096。
* __attention_probs_dropout_prob__ (_float_): 自注意力的dropout，默认为0.02。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码模式，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __hidden_dropout_prob__ (_float_): 输出的dropout，默认为0.1。
* __hidden_act__ (_str_): 激活函数，默认为relu。
* __computer_type__ (_mindspore.dtype_): 注意力计算的类型，默认为mstype.float32。

> **construct** (_hidden_states: mindspore.Tensor, attention_mask: mindspore.Tensor, enc_states: mindspore.Tensor,
> enc_attention_mask: mindspore.Tensor,seq_length: int, enc_seq_length: int_)

参数
* __hidden_states__ (_mindspore.Tensor_)：解码器的隐藏状态，shape为(batch_size, seq_len, hidden_size)。
* __attention_mask__ (_mindspore.Tensor_)：自注意力隐藏状态的mask矩阵（2D或者3D），值是[0/1]或者[True/False]，shape为(seq_len, seq_len)或者(batch_size, seq_len, seq_len)。
* __enc_states__ (_mindspore.Tensor_)：编码器的隐藏状态，shape为(batch_size, enc_seq_len, hidden_size)。
* __enc_attention_mask__ (_mindspore.Tensor_)：类似于attn_mask,但是用于enc_states。
* __seq_length__ (_int_)：解码器输入的长度。
* __ enc_seq_length__ (_int_)：编码器输入的长度。

返回
* __output__(_mindspore.Tensor_)：解码器最后输出表示。

> _class_ __mindtext.modules.decoder.seq2seq_decoder.TransformerDecoder__ (_batch_size: int, hidden_size: int, num_hidden_layers: int,
> num_attention_heads: int = 16, intermediate_size: int = 4096, attention_probs_dropout_prob: float = 0.1, use_one_hot_embeddings: bool = False,
> initializer_range: float = 0.02, hidden_dropout_prob: float = 0.1, hidden_act: str = "relu", compute_type: mindspore.dtype = mstype.float32_)

多层transformer解码器

> __init__ (_batch_size: int, hidden_size: int, num_hidden_layers: int, num_attention_heads: int = 16, intermediate_size: int = 4096,
> attention_probs_dropout_prob: float = 0.1, use_one_hot_embeddings: bool = False, initializer_range: float = 0.02, hidden_dropout_prob: float = 0.1,
> hidden_act: str = "relu", compute_type: mindspore.dtype = mstype.float32_)

参数
* __batch_size__ (_int_): 输入数据集的batch size。
* __hidden_size__ (_int_): 编码层的大小。
* __num_hidden_layers__ (_int_): 编码器的隐藏层数。
* __num_attention_heads__ (_int_): 每个编码层的注意力头数量，默认为16。
* __intermediate_size__ (_int_): 编码器中间层的大小，默认为4096。
* __attention_probs_dropout_prob__ (_float_): 自注意力的dropout，默认为0.1。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码模式，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __hidden_dropout_prob__ (_float_): 输出的dropout，默认为0.1。
* __hidden_act__ (_str_): 激活函数，默认为relu。
* __computer_type__ (_mindspore.dtype_): 注意力计算的类型，默认为mstype.float32。

> __construct__ (_hidden_states: mindspore.Tensor, attention_mask: mindspore.Tensor, enc_states: mindspore.Tensor,
> enc_attention_mask: mindspore.Tensor,seq_length: int, enc_seq_length: int_)

参数
* __hidden_states__ (_mindspore.Tensor_)：解码器的隐藏状态，shape为(batch_size, seq_len, hidden_size)。
* __attention_mask__ (_mindspore.Tensor_)：自注意力隐藏状态的mask矩阵（2D或者3D），值是[0/1]或者[True/False]，shape为(seq_len, seq_len)或者(batch_size, seq_len, seq_len)。
* __enc_states__ (_mindspore.Tensor_)：编码器的隐藏状态，shape为(batch_size, enc_seq_len, hidden_size)。
* __enc_attention_mask__ (_mindspore.Tensor_)：类似于attn_mask,但是用于enc_states。
* __seq_length__ (_int_)：解码器输入的长度。
* __ enc_seq_length__ (_int_)：编码器输入的长度。

返回
*__output__(_mindspore.Tensor_)：TransformerDecoder最后输出表示。

> _class_ __mindtext.modules.decoder.seq2seq_decoder.CreateAttentionMaskFromInputMask__ ()

根据输入的mask创建attention mask

> __init__ ()

> __construct__ (_input_mask: mindspore.Tensor_)

参数
* __input_mask__ (_mindspore.Tensor_): 输入序列的初始化mask

返回
* __attention_mask__ (_mindspore.Tensor_): 根据输入的input_mask创建的attention mask。

> _class_ __mindtext.modules.decoder.seq2seq_decoder.PredLogProbs__ (batch_size: int, width: int, compute_type: mindspore.dtype = mstype.float32,
> dtype: mindspore.dtype = mstype.float32)

得到log probs

> __init__ (_batch_size: int, width: int, compute_type: mindspore.dtype = mstype.float32, dtype: mindspore.dtype = mstype.float32_)

参数
* __batch_size__ (_int_)：batch_size。
* __width__ (_int_)：隐含状态大小。
* __compute_type__ (_mindspore.Tensor_)：计算类型，默认为mstype.float32。
* __dtype__ (_mindspore.dtype_)：计算log_softmax的计算类型，默认为mstype.float32。

> __construct__ (_input_tensor: mindspore.Tensor, output_weights: mindspore.Tensor, seq_length: int_)

参数
* __input_tensor__ (_mindspore.Tensor_)：执行对数概率计算的输入向量，在Transforme模型里就是解码器的输出。
* __output_weights__ (_mindspore.Tensor_)：output_weights，在Transforme模型里就是embedding的查找表。
* __seq_length__ (_int_)：输入向量的长度。

返回
* __output__ (_mindspore.Tensor_): 对数概率。

> _class_ __mindtext.modules.decoder.seq2seq_decoder.TransformerDecoderStep__ (_batch_size: int, hidden_size: int, max_decode_length: int,
> num_hidden_layers: int, num_attention_heads: int = 16, intermediate_size: int = 4096, attention_probs_dropout_prob: float = 0.3,
> use_one_hot_embeddings: bool = False, initializer_range: float = 0.02, hidden_dropout_prob: float = 0.3, hidden_act: str = "relu",
> compute_type: mindspore.dtype = mstype.float32, embedding_lookup: Optional[EmbeddingLookup] = None,  embedding_processor: Optional[EmbeddingPostprocessor] = None,
> projection: Optional[PredLogProbs] = None_)

多层transformer解码器步骤

> __init__ (_batch_size: int, hidden_size: int, max_decode_length: int, num_hidden_layers: int, num_attention_heads: int = 16,
> intermediate_size: int = 4096, attention_probs_dropout_prob: float = 0.3, use_one_hot_embeddings: bool = False, initializer_range: float = 0.02,
> hidden_dropout_prob: float = 0.3, hidden_act: str = "relu", compute_type: mindspore.dtype = mstype.float32, embedding_lookup: Optional[EmbeddingLookup] = None,
> embedding_processor: Optional[EmbeddingPostprocessor] = None, projection: Optional[PredLogProbs] = None_)

参数
* __batch_size__ (_int_): 输入数据集的batch size。
* __hidden_size__ (_int_): 编码层的大小。
* __max_decode_length__ (_int_): 最大解码长度。
* __num_hidden_layers__ (_int_): 编码器的隐藏层数。
* __num_attention_heads__ (_int_): 每个编码层的注意力头数量，默认为16。
* __intermediate_size__ (_int_): 编码器中间层的大小，默认为4096。
* __attention_probs_dropout_prob__ (_float_): 自注意力的dropout，默认为0.1。
* __use_one_hot_embeddings__ (_bool_): 是否使用one hot编码模式，默认为False。
* __initializer_range__ (_float_): 截断正态分布的初始值，默认为0.02。
* __hidden_dropout_prob__ (_float_): 输出的dropout，默认为0.1。
* __hidden_act__ (_str_): 激活函数，默认为relu。
* __computer_type__ (_mindspore.dtype_): 注意力计算的类型，默认为mstype.float32。
* __embedding_lookup__ (_Optional[EmbeddingLookup]_): embedding查找模块。
* __hidden_act__ (_Optional[EmbeddingPostprocessor]_): embedding后处理模块。
* __hidden_act__ (_Optional[PredLogProbs]_): 对数概率模块。

> __construct__ (_input_ids: mindspore.Tensor, enc_states: mindspore.Tensor, enc_attention_mask: mindspore.Tensor,seq_length: int_)

参数
* __input_ids__ (_mindspore.Tensor_)：输入语句的单词在词典中的索引序号，shape为(batch_size, beam_width)。
* __enc_states__ (_mindspore.Tensor_)：编码器的隐藏状态，shape为(batch_size, enc_seq_len, hidden_size)。
* __enc_attention_mask__ (_mindspore.Tensor_)：自注意力隐藏状态的mask矩阵（2D或者3D），值是[0/1]或者[True/False]，shape为(seq_len, seq_len)或者(batch_size, seq_len, seq_len)。
* __seq_length__ (_int_)：解码器输入的长度。

返回
* __output__ (_mindspore.Tensor_): 对数概率。

> _class_ __mindtext.modules.decoder.seq2seq_decoder.LengthPenalty__ (_weight: float = 1.0, compute_type: mindspore.dtype = mstype.float32_)

根据翻译的长度将分数标准化

> __init__ (_weight: float = 1.0, compute_type: mindspore.dtype = mstype.float32_)

参数
* __weight__ (_float_): 长度惩罚的权重，默认为1.0。
* __compute_type__ (_mindspore.dtype_): Transformer的计算类型，默认为mstype.float32。

> __construct__ (_length_tensor: mindspore.Tensor_)

参数
* __length_tensor__ (_mindspore.Tensor_): 隐藏状态的长度。

返回
* __output__ (_mindspore.Tensor_): 长度惩罚的结果。

> _class_ __mindtext.modules.decoder.seq2seq_decoder.TileBeam__ (_beam_width: int_)

> __init__ (_beam_width: int_)

参数
* __beam_width__ (_int_): Beam宽度设置，默认为4。

> __construct__ (_input_tensor: mindspore.Tensor_)

参数
* __input_tensor__ (_mindspore.Tensor_): shape为(batch, dim1, dim2)。

返回
* __output__ (_mindspore.Tensor_): shape为(batch*beam, dim1, dim)。

> _class_ __mindtext.modules.decoder.seq2seq_decoder.Mod__ (_compute_type: mindspore.dtype = mstype.float32_)

mod函数

> __init__ (_compute_type: mindspore.dtype = mstype.float32_)

参数
* __compute_type__ (_mindspore.dtype_): Transformer的计算类型，默认为mstype.float32。

> __construct__ (_input_x: mindspore.Tensor, input_y: mindspore.Tensor_)

参数
* __input_x__ (_mindspore.Tensor_): Mod函数的input_x向量。
* __input_y__ (_mindspore.Tensor_): Mod函数的input_y向量。


返回
* __x__ (_mindspore.Tensor_): Mod函数的结果。

> _class_ __mindtext.modules.decoder.seq2seq_decoder.BeamSearchDecoder__ (_batch_size: int, seq_length: int, vocab_size: int, decoder: TransformerDecoderStep,
> beam_width: int = 4, length_penalty_weight: float = 1.0, max_decode_length: int = 128, sos_id: int = 1, eos_id: int = 2, compute_type: mindspore.dtype = mstype.float32_)

Beam search 解码器

> __init__ (_batch_size: int, seq_length: int, vocab_size: int, decoder: TransformerDecoderStep, beam_width: int = 4, length_penalty_weight: float = 1.0,
> max_decode_length: int = 128, sos_id: int = 1, eos_id: int = 2, compute_type: mindspore.dtype = mstype.float32_)

参数
* __batch_size__ (_int_): 数据集的batch size。
* __seq_length__ (_int_): 序列的长度。
* __vocab_size__ (_int_): 字典的大小。
* __decoder__ (_TransformerDecoderStep_): 解码模块。
* __beam_width__ (_int_): beam宽度设置，默认为4。
* __length_penalty_weight__ (_int_): 长度惩罚权重，默认为1.0。
* __max_decode_length__ (_int_): 最大解码长度，默认为128。
* __sos_id__ (_int_): 序列开始token的ID，默认为1。
* __eos_id__ (_int_): 序列结束token的ID，默认为2。
* __compute_type__ (_mindspore.dtype_): Transformer的计算类型，默认为mstype.float32。

> __one_setp__ (_cur_input_ids: mindspore.Tensor, enc_states: mindspore.Tensor, enc_attention_mask: mindspore.Tensor, state_log_probs: mindspore.Tensor, state_seq: mindspore.Tensor,
> state_finished: mindspore.Tensor, state_length: mindspore.Tensor_)

解码器的一步

参数
* __cur_input_ids__ (_mindspore.Tensor_)：输入语句的单词在词典中的索引序号。
* __enc_states__ (_mindspore.Tensor_)：编码器的隐藏状态，shape为(batch_size, enc_seq_len, hidden_size)。
* __enc_attention_mask__ (_mindspore.Tensor_)：自注意力隐藏状态的mask矩阵（2D或者3D），值是[0/1]或者[True/False]，shape为(seq_len, seq_len)或者(batch_size, seq_len, seq_len)。
* __state_log_probs__ (_int_)：序列隐含状态的对数概率。
* __state_seq__ (_mindspore.Tensor_)：序列隐含状态。
* __state_finished__ (_mindspore.Tensor_)：序列隐含状态的结束flag。
* __state_length__ (_int_)：序列隐含状态的长度。

返回
* __cur_input_ids__ (_mindspore.Tensor_): one_step的结果。
* __state_log_probs__ (_mindspore.Tensor_): one_step的结果。
* __state_seq__ (_mindspore.Tensor_): one_step的结果。
* __state_finished__ (_mindspore.Tensor_): one_step的结果。
* __state_length__ (_mindspore.Tensor_): one_step的结果。

> __construct__ (_enc_states: mindspore.Tensor, enc_attention_mask: mindspore.Tensor_)

参数
* __enc_states__ (_mindspore.Tensor_): 编码器的隐藏状态，shape为(batch_size, enc_seq_len, hidden_size)。
* __enc_attention_mask__ (_mindspore.Tensor_): 自注意力隐藏状态的mask矩阵（2D或者3D），值是[0/1]或者[True/False]，shape为(seq_len, seq_len)或者(batch_size, seq_len, seq_len)。


返回
* __predicted_ids__ (_mindspore.Tensor_): 预测的索引号。