mindtext.embeddings
============

.. automodule:: mindtext.modules.decoder

子模块
------

.. toctree::
   :maxdepth: 1

   mindtext.modules.decoder.norm_decoder
   mindtext.modules.decoder.seq2seq_decoder

