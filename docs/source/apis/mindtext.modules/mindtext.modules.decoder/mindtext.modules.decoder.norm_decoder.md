# mindtext.modules.decoder.norm_decoder

> _class_ __mindtext.modules.decoder.norm_decoder.NormalDecoder__ _(num_filters: int, num_classes: int, 
> classes_dropout: float = 0.1, activation: str = 'relu')_

使用一个dropout层和一个全连接层(dense layer)来进行解码。

Example
```
>>> 
```

> __init__ (_num_filters: int, num_classes: int, classes_dropout: float = 0.1, 
> activation: str = 'relu'_)

参数 
* __num_filters__ (_int_): 过滤器的数量，默认为256。
* __num_classes__ (_int_): 输出分类的数量。
* __classes_dropout__ (_float_): 对词嵌入进行dropout，默认为0.1。
* __activation__ (_str_): 激活函数，默认为relu。

> __construct__ (_x: mindspore.Tensor_)

使用解码层进行解码

参数
* __x__ (_mindspore.Tensor_)：输入的向量。

返回
* __x__(_mindspore.Tensor_)：解码器最后输出表示。