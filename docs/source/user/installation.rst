===============
安装指南
===============
 
.. contents::
   :local:

mindtext 依赖如下包::

    PyYAML==5.4.1
    scikit_learn==0.24.1
    six==1.16.0
    pandas==1.2.4
    transformers==4.10.0
    spacy==3.1.2
    datasets==1.12.0
    tqdm==4.62.2
    requests==2.25.1
    numpy==1.21.2
    mindspore_gpu==1.3.0

其中`mindspore`的安装可能与操作系统及 `CUDA` 的版本相关，请参见 `MindSpore 官网 <https://www.mindspore.cn/>`_ 。
在依赖包安装完成的情况，您可以在命令行执行如下指令完成mindtext的安装

..  code:: shell

   >>> git clone https://gitee.com/mindspore/mindtext.git
   >>> cd mindtext
   >>> python setup.py install
