===============
快速入门
===============
 
如果你想用 mindtext 来快速地解决某类 NLP 问题，你可以参考以下教程之一：

.. toctree::
   :maxdepth: 1

   /tutorials/getting_started_1

这些教程是简单地介绍了 mindtext 的使用流程，其中文本分类相对简单，序列标注则较为复杂。

更多的教程分析见

:doc:`mindtext详细教程 </user/tutorials>`

