﻿========================
mindtext 详细使用教程
========================

这里是更详细的使用教程。对于大部分的用户，我们建议你从第一篇开始顺序阅读；如果你只想了解其中的一部分，也可以进行选读。
 
.. toctree::
   :maxdepth: 1

   使用dataset构建并处理数据集 </tutorials/tutorial_1_data_preprocess>
   使用vocabulary构建词典 </tutorials/tutorial_2_vocabulary>
   使用Embedding将文本转化为词向量 </tutorials/tutorial_3_embedding>
   构建优化器 </tutorials/tutorial_4_build_optimizer>
   构建损失函数 </tutorials/tutorial_5_loss_optimizer>
   构建配置文件 </tutorials/tutorial_6_build_profile>
   使用engine训练 </tutorials/tutorial_7_train_engine>
   使用engine预测 </tutorials/tutorial_8_pred_engine>
   使用engine评估 </tutorials/tutorial_9_eval_engine>
   自定义数据集</tutorials/tutorial_10_custom_dataset>
   自定义优化器</tutorials/tutorial_11_custom_optimizer>

