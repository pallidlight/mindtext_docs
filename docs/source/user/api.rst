﻿========================
mindtext API
========================

这里是更详细的使用教程，可以选择你只想了解其中的一部分进行选读。

.. toctree::
   :maxdepth: 2

   mindtext.common </apis/common.*>
   mindtext.embeddings </apis/embeddings.*>
   mindtext.dataset </apis/dataset.*>
   mindtext.modules </apis/modules>
