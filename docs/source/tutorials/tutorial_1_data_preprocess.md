# 使用dataset构建并处理数据集
<font  size=4>dataset在 mindtext/mindtext/dataset包目录下。该目录包含classification，generationpair_classification，reading__comprehension，regression和tagging文件夹。每个文件夹下包含对应分类的dataset数据集的子类，根据需求导入对应分类的dataset数据集子类。



dataset的初始化如下面的Example所示:

### 加载数据集
```python
>>>from  mindtext.mindtext.dataset.classification import cola

>>> cola = CoLADataset(path='./cola',tokenizer='xlnet-base-cased', lang='en',batch_size=64,max_length=10,truncation_strategy=True,columns_list=['input_ids','type_token_ids','attention_mask','label']
        ,test_columns_list=['input_ids','type_token_ids','attention_mask','label'])
>>> dataset = cola()
 
```
部分参数解释以及可选值

1. tokenizer可选值有：

 -  spacy（英文）：默认采用 ’en_core_web_sm’。
 -     raw（英文）：以空格为分隔符进行分词。
 -     cn-char（中文）：将每个单字分割开。
 - 	预训练模型分词器：依赖于transformers组件，可以选择transformers组件中已有	的分词器，例如传入`’bert-base-cased’`则使用bert预训练模型的分词器对数据进行处理。

2.	stream：是否以数据流的方式处理并写入mindrecord数据文件，当内存空间有限时建议设置为True

3.	max_length,[max_pair_length]：输入数据的最大长度。
当数据集是CLSBaseDataset的子类（文本分类任务）时，指定该值会将输入样本padding到max_length，若不指定则将输入样本padding到数据集样本最大长度；
当数据集是PairCLSBaseDataset的子类（文本匹配任务）时，并且tokenizer为预训练模型分词器时，指定该值会将输入样本两段序列拼接为一句后再padding到max_length，反之padding到数据集样本最大长度；若tokenizer不为预训练模型分词器，则可以指定max_pair_length为第二段序列设置最大长度，处理方式同上。
当数据集是GenerateBaseDataset的子类（生成任务）时，可以同时指定max_length与max_pair_length，处理方式如上。

4.	truncation_strategy：裁剪序列，若指定的max_length,[max_pair_length]小于数据集样本最大长度，则需要将该值指定为True，当使用预训练模型分词器时，具体可选值可以参考`transformers.PretrainedTokenizerBase`的`truncate`参数。

5.	columns_list：指定MindDataset的列名，需要与训练集、验证集数据一致：
当数据集是CLSBaseDataset的子类（文本分类任务），并且tokenzier为预训练模型分词器时，该值一般可以指定为`[‘input_ids’,’token_type_ids’,’attention_mask’,’label’]`，反之一般可以指定为`[‘input_ids’,’input_length’,’label’]`。
当数据集是PairCLSBaseDataset的子类（文本匹配任务），并且tokenzier为预训练模型分词器时，该值一般可以指定为`[‘input_ids’,’token_type_ids’,’attention_mask’,’label’]`，反之一般可以指定为`[‘input1_ids’,’input1_length’,‘input2_ids’,’input2_length’,’label’]`。
当数据集是GenerateBaseDataset的子类（生成任务），并且tokenzier为预训练模型分词器时，该值一般可以指定为`[‘input_ids’,’token_type_ids’,’attention_mask’,’labels’]`。

6. test_columns_list：指定MindDataset的列名，需要与测试集数据一致。
columns_list与test_columns_list通常用于排列数据列顺序，以匹配模型的输入参数

### 迭代数据集

```
for data in dataset.create_dict_iterator():
    print("data shape: {}".format(data['input_ids'].shape), ", Label: {}".format(data['label']))
```
```
shape: (64, 10, 2) , Label: 0
shape: (64, 10, 2) , Label: 0
shape: (64, 10, 2) , Label: 1
shape: (64, 10, 2) , Label: 0
shape: (32, 10, 2) , Label: 1
```
### 读取缓存中的mindrecord文件构造数据集

```
因为加载数据集的同时，会对数据进行预处理并且写入mindrecored文件到~/.mindtext/dataset（缓存）中
所以我们为了节省读取时间，可以直接读取缓存中的mindrecored文件到dataset中。
dataset=Coladataset()
ds=dataset.from_cache(columns_list=['input_ids','type_token_ids','attention_mask','label'],test_columns_list=['input_ids','type_token_ids','attention_mask','label'],batch_size=64)
```
<font>