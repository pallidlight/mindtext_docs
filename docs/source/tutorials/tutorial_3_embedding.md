<font  size=4>

# 使用StaticEmbedding将文本转化为向量

根据输入的文本从预训练的模型中提取相应的embedding。如果未找到embedding，则使用随机embedding对其进行初始化。

参数:
vocab(Vocabulary): StaticEmbedding将只加载所包含单词的单词向量在单词列表中，如果在预训练的embeding中没有找到，则使用随机初始化。

model_dir_or_name(Union[str, None]):有两种方法调用预训练的静态embedding:第一种方法是传入本地文件夹(应该只有一个后缀为.txt的文件)或文件路径。第二个是传递的嵌入的名称。在第二种情况下，嵌入将自动检查模型是否存在于缓存中。如果没有，该embedding将会自动下载(在华为云计算实现)。如果输入为None，则使用维度

embeddding_dim随机初始化嵌入。

embeddding_path (Union[str, None]):嵌入文件的路径。

embeddding_dim (int):随机初始化嵌入的尺寸。如果值大于0,Model_dir_or_name将被忽略。

requires_grad (bool,可选):默认值:真的。

dropout(float，可选):dropout层表示嵌入的概率。
```
Examples:
        >>> vocab = Vocabulary()
        >>> vocab.update(["i", "am", "fine"])
        >>> embed = StaticEmbedding(vocab, model_dir_or_name=None, embedding_dim=5)
        >>> words = mindspore.Tensor([[vocab[word] for word in ["i", "am", "fine"]]])
        >>> embed(words)
        >>> Tensor(shape=[1, 3, 5], dtype=Float32, value=
            [[[7.56267071e-001, -3.02625038e-002, 6.10783875e-001, 4.03315663e-001, -6.82987273e-001],
            [3.35875869e-001, 2.93195043e-002, 2.17977986e-001, 9.68403295e-002, -4.01605248e-001],
            [-2.35586300e-001, 4.89649743e-001, -2.10691467e-001, -1.81295246e-001, -6.90823942e-002]]]).

```
<font>