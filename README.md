# 快速入门 mindtext 文档编写

本教程为 mindtext 文档编写者创建，文档编写者包括合作开发人员和文档维护人员。您在一般情况下属于前者，
只需要了解整个框架的部分内容即可。

本文档的在线查看:[mindtext在线文档](https://mindtext.readthedocs.io/en/latest/)



## 合作开发人员

mindtext 的文档使用基于[reStructuredText标记语言](http://docutils.sourceforge.net/rst.html)的
[Sphinx](http://sphinx.pocoo.org/)工具生成，由[Read the Docs](https://readthedocs.org/)网站自动维护生成。
一般开发者只要编写符合reStructuredText语法规范的文档并通过[PR](https://help.github.com/en/articles/about-pull-requests)，
就可以为mindtext的文档贡献一份力量。

如果你想在本地编译文档并进行大段文档的编写，您需要安装Sphinx工具以及sphinx-rtd-theme主题：
```bash
mindtext/docs> pip install sphinx
mindtext/docs> pip install sphinx-rtd-theme
```
然后在本目录下执行 `make html` 命令。可以看到转换的html文件

我们在[这里](./source/user/example.rst)列举了mindtext文档经常用到的reStructuredText语法（网页查看请结合Raw模式），
您可以通过阅读它进行快速上手。mindtext大部分的文档都是写在代码中通过Sphinx工具进行抽取生成的。

## 文档维护人员

文档维护人员需要了解 Makefile 中全部命令的含义，并了解到目前的文档结构
是在 sphinx-apidoc 自动抽取的基础上进行手动修改得到的。
文档维护人员应进一步提升整个框架的自动化程度，并监督合作开发人员不要破坏文档项目的整体结构。